# ens-scan

A project designed to showcase:

- Development and consumption of [The Graph](https://thegraph.com) subgraphs
- [NX Incremental builds](https://nx.dev/more-concepts/incremental-builds), with applications serving mostly just for linking libraries
- [NX Project Target Configurators](https://nx.dev/recipes/advanced-plugins/project-inference-plugins#implementing-a-project-target-configurator)
- GraphQL federation with [GraphQL Mesh](https://the-guild.dev/graphql/mesh)

## Local development

### Prerequisites

- [Node](https://nodejs.org/) 18
- The Graph [Hosted Service](https://thegraph.com/hosted-service) subgraph
- [Infura](https://www.infura.io/) project

### Bootstrapping

#### Install deps

```bash
$ npm i
```

#### Set up `.env`

Copy from the `.env.example`, replacing placeholder values in

- `NX_APP_SUBGRAPH_URL`
- `NX_APP_INFURA_URL`
- `SUBGRAPH_NAME`

#### Authenticate the Graph CLI

Using the auth token for the created subgraph, run

```bash
$ npm run -- graph --product hosted-service AUTH_TOKEN
```

#### Deploy the subgraph, and wait for it to be in sync

For the time being, there's no local subgraph development environment, so this step is required to run the GraphQL gateway

```bash
$ npm run nx run subgraph:deploy
```

### Starting the applications

#### Start the backend

```bash
$ npm run nx run backend:serve
```

#### Start the mesh gateway

```bash
$ npm run nx run mesh-gateway:serve
```

#### Start the frontend

```bash
$ npm run nx run frontend:serve
```
