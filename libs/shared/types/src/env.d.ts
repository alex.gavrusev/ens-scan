export {};

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NX_APP_SUBGRAPH_URL: string;
      NX_APP_BACKEND_URL: string;
      NX_APP_MESH_GATEWAY_URL: string;
      NX_APP_INFURA_URL: string;
    }
  }
}
