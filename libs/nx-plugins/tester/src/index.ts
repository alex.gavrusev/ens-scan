import { ProjectTargetConfigurator } from '@nrwl/devkit';
import { dirname } from 'path';
import chalk from 'chalk';

export const projectFilePatterns: string[] = ['jest.config.ts'];

const logAdd = (file: string) =>
  console.log(
    `${chalk.green.bold('[nx-plugins/tester]')} ${chalk.white(
      `Providing default test target to ${file}`
    )}`
  );

export const registerProjectTargets: ProjectTargetConfigurator = (file) => {
  const projectDir = dirname(file);

  logAdd(projectDir);

  return {
    test: {
      executor: '@nrwl/jest:jest',
      outputs: ['{workspaceRoot}/coverage/{projectRoot}'],
      options: {
        jestConfig: `${projectDir}/jest.config.ts`,
        passWithNoTests: true,
      },
      configurations: {
        watch: {
          watch: true,
        },
      },
    },
  };
};
