# nx-plugins-tester

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build nx-plugins-tester` to build the library.

## Running unit tests

Run `nx test nx-plugins-tester` to execute the unit tests via [Jest](https://jestjs.io).
