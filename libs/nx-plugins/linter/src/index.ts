import { ProjectTargetConfigurator } from '@nrwl/devkit';
import { dirname } from 'path';
import chalk from 'chalk';

export const projectFilePatterns: string[] = ['.eslintrc.json'];

const logAdd = (file: string) =>
  console.log(
    `${chalk.green.bold('[nx-plugins/linter]')} ${chalk.white(
      `Providing default lint and lint:fix targets to ${file}`
    )}`
  );

const getLintFilePatterns = (projectDir: string): string[] => {
  if (projectDir.includes('nx-plugins')) {
    // nx-plugins should also lint their json configs
    return [
      `${projectDir}/**/*.ts`,
      `${projectDir}/generators.json`,
      `${projectDir}/executors.json`,
      `${projectDir}/package.json`,
    ];
  }

  return [`${projectDir}/**/*.{ts,tsx,js,jsx}`];
};

export const registerProjectTargets: ProjectTargetConfigurator = (file) => {
  const projectDir = dirname(file);

  logAdd(projectDir);

  return {
    lint: {
      executor: '@nrwl/linter:eslint',
      outputs: ['{options.outputFile}'],
      options: {
        lintFilePatterns: getLintFilePatterns(projectDir),
      },
      configurations: {
        fix: {
          fix: true,
        },
      },
    },
  };
};
