# nx-plugins-linter

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build nx-plugins-linter` to build the library.

## Running unit tests

Run `nx test nx-plugins-linter` to execute the unit tests via [Jest](https://jestjs.io).
