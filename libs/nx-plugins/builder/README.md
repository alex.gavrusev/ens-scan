# nx-plugins-builder

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build nx-plugins-builder` to build the library.

## Running unit tests

Run `nx test nx-plugins-builder` to execute the unit tests via [Jest](https://jestjs.io).

## `package.json` `main` note

Since this plugin also adds its own build target, NX is unable to resolve its compiled `index.js`.
Thus, `main` points to the typescript entrypoint file, `index.ts`
