import { ProjectTargetConfigurator } from '@nrwl/devkit';

import {
  nxPluginTargetConfigurator,
  webAppTargetConfigurator,
  webLibTargetConfigurator,
  nodeAppTargetConfigurator,
  nodeLibTargetConfigurator,
  configTargetConfigurator,
} from './target-configurators';

export const projectFilePatterns: string[] = ['project.json'];

export const registerProjectTargets: ProjectTargetConfigurator = (file) => {
  /**
   * NOTE: trailing slash as to not include frontend-e2e
   */
  if (file.includes('apps/frontend/')) {
    return webAppTargetConfigurator(file);
  }

  /**
   * NOTE(regex): msw and types got no build
   */
  if (/libs\/frontend\/(?!msw|types)/gi.test(file)) {
    return webLibTargetConfigurator(file);
  }

  if (/apps\/(backend|mesh-gateway)/gi.test(file)) {
    return nodeAppTargetConfigurator(file);
  }

  if (file.includes('libs/backend')) {
    return nodeLibTargetConfigurator(file);
  }

  if (file.includes('nx-plugins')) {
    return nxPluginTargetConfigurator(file);
  }

  if (file.includes('libs/config')) {
    return configTargetConfigurator(file);
  }

  return {};
};
