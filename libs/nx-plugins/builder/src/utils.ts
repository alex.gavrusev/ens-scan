import chalk from 'chalk';

export const logAdd = (file: string) =>
  console.log(
    `${chalk.green.bold('[nx-plugins/builder]')} ${chalk.white(
      `Providing default build target to ${file}`
    )}`
  );
