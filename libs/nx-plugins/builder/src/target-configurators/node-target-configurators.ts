import { ProjectTargetConfigurator } from '@nrwl/devkit';
import { dirname, join } from 'path';

import { logAdd } from '../utils';

const webpackBuildConfigurations = {
  development: {
    sourceMap: true,

    buildLibsFromSource: true,
    optimization: false,
    extractLicenses: false,
  },
  production: {
    sourceMap: false,

    buildLibsFromSource: false,
    optimization: true,
    extractLicenses: true,
    generatePackageJson: true,
  },
};

export const nodeAppTargetConfigurator: ProjectTargetConfigurator = (file) => {
  const appDir = dirname(file);

  logAdd(appDir);

  return {
    build: {
      executor: '@nrwl/webpack:webpack',
      outputs: ['{options.outputPath}'],
      options: {
        target: 'node',
        outputPath: join('dist', appDir),
        main: join(appDir, 'src', 'main.ts'),
        tsConfig: join(appDir, 'tsconfig.app.json'),
        assets: [
          {
            input: join(appDir, 'src', 'assets'),
            glob: 'vercel.json',
            output: '.',
          },
          {
            input: join(appDir, 'src', 'assets'),
            glob: '!(vercel.json)',
            output: 'assets',
          },
        ],
        compiler: 'swc',
        webpackConfig: 'libs/config/build/src/node/webpack.app.ts',
      },
      defaultConfiguration: 'production',
      configurations: webpackBuildConfigurations,
    },
  };
};

export const nodeLibTargetConfigurator: ProjectTargetConfigurator = (file) => {
  const libDir = dirname(file);

  logAdd(libDir);

  return {
    build: {
      executor: '@nrwl/js:swc',
      outputs: ['{options.outputPath}'],
      options: {
        outputPath: join('dist', libDir),
        main: join(libDir, 'src', 'index.ts'),
        tsConfig: join(libDir, 'tsconfig.lib.json'),
        swcrc: 'libs/config/build/src/node/.swcrc.json',
      },
    },
  };
};
