import { ProjectTargetConfigurator } from '@nrwl/devkit';
import { dirname, join } from 'path';

import { logAdd } from '../utils';

export const configTargetConfigurator: ProjectTargetConfigurator = (file) => {
  const libDir = dirname(file);

  logAdd(libDir);

  return {
    build: {
      executor: '@nrwl/js:swc',
      outputs: ['{options.outputPath}'],
      options: {
        outputPath: join('dist', libDir),
        main: join(libDir, 'src', 'index.ts'),
        tsConfig: join(libDir, 'tsconfig.lib.json'),
        swcrc: 'libs/config/build/src/node/.swcrc.json',
        assets: [
          `${libDir}/*.md`,
          {
            input: `./${libDir}/src`,
            glob: '**/*.json',
            output: './src',
          },
        ],
      },
    },
  };
};
