import { ProjectTargetConfigurator } from '@nrwl/devkit';
import { dirname, join } from 'path';

import { logAdd } from '../utils';

export const nxPluginTargetConfigurator: ProjectTargetConfigurator = (file) => {
  const pluginDir = dirname(file);

  logAdd(pluginDir);

  return {
    build: {
      executor: '@nrwl/js:swc',
      outputs: ['{options.outputPath}'],
      options: {
        outputPath: join('dist', pluginDir),
        main: join(pluginDir, 'src', 'index.ts'),
        tsConfig: join(pluginDir, 'tsconfig.lib.json'),
        swcrc: 'libs/config/build/src/node/.swcrc.json',
        assets: [
          `${pluginDir}/*.md`,
          {
            input: `./${pluginDir}/src`,
            glob: '**/!(*.ts)',
            output: './src',
          },
          {
            input: `./${pluginDir}/src`,
            glob: '**/*.d.ts',
            output: './src',
          },
          {
            input: `./${pluginDir}`,
            glob: 'generators.json',
            output: '.',
          },
          {
            input: `./${pluginDir}`,
            glob: 'executors.json',
            output: '.',
          },
        ],
      },
    },
  };
};
