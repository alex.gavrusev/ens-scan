import { ProjectTargetConfigurator } from '@nrwl/devkit';
import { dirname, join } from 'path';

import { logAdd } from '../utils';

const webpackBuildConfigurations = {
  development: {
    sourceMap: true,
    vendorChunk: true,

    buildLibsFromSource: true,
    optimization: false,
    outputHashing: 'none',
    namedChunks: false,
    extractLicenses: false,
  },
  production: {
    sourceMap: false,
    vendorChunk: false,

    buildLibsFromSource: false,
    optimization: true,
    outputHashing: 'all',
    namedChunks: true,
    extractLicenses: true,
  },
};

export const webAppTargetConfigurator: ProjectTargetConfigurator = (file) => {
  const appDir = dirname(file);

  logAdd(appDir);

  return {
    build: {
      executor: '@nrwl/webpack:webpack',
      outputs: ['{options.outputPath}'],
      options: {
        outputPath: join('dist', appDir),
        index: join(appDir, 'src', 'index.html'),
        main: join(appDir, 'src', 'main.tsx'),
        tsConfig: join(appDir, 'tsconfig.app.json'),
        assets: [
          join(appDir, 'src', 'favicon.ico'),
          {
            input: join(appDir, 'src', 'assets'),
            glob: 'vercel.json',
            output: '.',
          },
          {
            input: join(appDir, 'src', 'assets'),
            glob: '!(vercel.json)',
            output: 'assets',
          },
        ],
        compiler: 'swc',
        webpackConfig: 'libs/config/build/src/web/webpack.app.ts',
      },
      defaultConfiguration: 'production',
      configurations: webpackBuildConfigurations,
    },
  };
};

export const webLibTargetConfigurator: ProjectTargetConfigurator = (file) => {
  const libDir = dirname(file);

  logAdd(libDir);

  return {
    build: {
      executor: '@nrwl/js:swc',
      outputs: ['{options.outputPath}'],
      options: {
        outputPath: join('dist', libDir),
        main: join(libDir, 'src', 'index.ts'),
        tsConfig: join(libDir, 'tsconfig.lib.json'),
        swcrc: 'libs/config/build/src/web/.swcrc.json',
      },
    },
  };
};
