import { AnyVariables, UseQueryResponse, UseQueryState } from 'urql';

export const useQueryErrorHandling = <
  Data,
  Variables extends AnyVariables = AnyVariables
>(
  response: UseQueryResponse<Data, Variables>
) => {
  const [{ error, data }] = response;

  if (error) {
    throw error;
  }

  // data should always be defined when in suspense
  if (!data) {
    throw new Error('Unable to fetch');
  }

  return response as [
    Omit<UseQueryState<Data, Variables>, 'error'> & { data: Data },
    UseQueryResponse<Data, Variables>[1]
  ];
};
