import { getJestConfig } from '@ens-scan/config/test/web';

export default getJestConfig('frontend-graphql-hooks');
