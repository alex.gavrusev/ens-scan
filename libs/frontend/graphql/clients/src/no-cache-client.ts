import {
  AnyVariables,
  Client,
  dedupExchange,
  fetchExchange,
  GraphQLRequest,
  Operation,
  OperationContext,
  OperationType,
} from 'urql';

import { sharedOptions } from './shared-opts';

class NoCacheClient extends Client {
  override createRequestOperation<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Data = any,
    Variables extends AnyVariables = AnyVariables
  >(
    kind: OperationType,
    request: GraphQLRequest<Data, Variables>,
    opts?: Partial<OperationContext> | undefined
  ): Operation<Data, Variables> {
    const op = super.createRequestOperation(kind, request, opts);

    op.context.requestPolicy = 'network-only';

    return op;
  }
}

export const createNoCacheUrqlClient = (): Client =>
  new NoCacheClient({
    ...sharedOptions,
    exchanges: [dedupExchange, fetchExchange],
  });
