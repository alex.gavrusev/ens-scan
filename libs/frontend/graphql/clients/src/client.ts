import { dedupExchange, createClient, fetchExchange } from 'urql';
import { cacheExchange } from '@urql/exchange-graphcache';

import schema from '@ens-scan/frontend/graphql/codegen/schema';

import { sharedOptions } from './shared-opts';

export const urqlClient = createClient({
  ...sharedOptions,
  exchanges: [dedupExchange, cacheExchange({ schema }), fetchExchange],
});
