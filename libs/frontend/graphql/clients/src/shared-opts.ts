import { ClientOptions } from 'urql';

export const sharedOptions: ClientOptions = {
  url: process.env.NX_APP_MESH_GATEWAY_URL,
  suspense: true,
};
