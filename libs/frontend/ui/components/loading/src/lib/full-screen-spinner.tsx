import { Flex, Spinner } from '@chakra-ui/react';
import { FC } from 'react';

export const FullScreenSpinner: FC = () => (
  <Flex w="full" h="100vh" justify="center" align="center">
    <Spinner size="xl" />
  </Flex>
);
