import { FC, ReactNode, Suspense } from 'react';

import { FullScreenSpinner } from './full-screen-spinner';

export type ScreenSuspense = {
  children: ReactNode;
};

export const ScreenSuspense: FC<ScreenSuspense> = ({ children }) => (
  <Suspense fallback={<FullScreenSpinner />}>{children}</Suspense>
);
