import { FC } from 'react';
import { Text, Flex, Link, Icon } from '@chakra-ui/react';
import { FaExternalLinkAlt } from '@react-icons/all-files/fa/FaExternalLinkAlt';
import { U } from 'ts-toolbelt';

type DomainOwnerLinkProps = {
  name: string;
  ethereumName: U.Nullable<string>;
};

export const DomainOwnerLink: FC<DomainOwnerLinkProps> = ({
  name,
  ethereumName,
}) => (
  <Text>
    <Text as="span" fontWeight="bold">
      Owner:{' '}
    </Text>
    <Flex
      display="inline-flex"
      align="center"
      as={Link}
      isExternal
      href={`https://etherscan.io/address/${name}`}
    >
      {ethereumName ?? name}
      <Icon as={FaExternalLinkAlt} ml="1" />
    </Flex>
  </Text>
);
