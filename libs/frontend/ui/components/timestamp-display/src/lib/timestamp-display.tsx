import { FC } from 'react';
import { Flex, Icon, Text } from '@chakra-ui/react';
import { FaRegClock } from '@react-icons/all-files/fa/FaRegClock';
import dayjs from 'dayjs';

export type TimestampDisplayProps = {
  timestamp: number | string;
  includeYear?: boolean;
};

export const TimestampDisplay: FC<TimestampDisplayProps> = ({
  timestamp,
  includeYear,
}) => (
  <Flex display="inline-flex" align="center">
    <Icon mr={1} as={FaRegClock} />

    <Text>
      {dayjs
        .unix(Number(timestamp))
        .format(`${includeYear ? 'YYYY.' : ''}DD.MM, HH:mm:ss`)}
    </Text>
  </Flex>
);
