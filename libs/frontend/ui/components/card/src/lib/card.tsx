import { FC, ReactNode } from 'react';
import { Box, LayoutProps, SpaceProps } from '@chakra-ui/react';

export type CardProps = { children: ReactNode } & LayoutProps & SpaceProps;

export const Card: FC<CardProps> = (props) => (
  <Box
    borderRadius="lg"
    borderColor="gray.400"
    borderWidth={1}
    w="full"
    bgColor="white"
    {...props}
  />
);
