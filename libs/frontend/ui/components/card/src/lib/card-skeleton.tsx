import { LayoutProps, Skeleton } from '@chakra-ui/react';
import { FC } from 'react';

export type CardSkeletonProps = {
  h: LayoutProps['h'];
};

export const CardSkeleton: FC<CardSkeletonProps> = (props) => (
  <Skeleton borderRadius="lg" {...props} />
);
