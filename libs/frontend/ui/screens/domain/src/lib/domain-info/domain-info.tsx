import { FC } from 'react';
import { gql } from 'urql';
import { Heading, Text, VStack, Flex } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';

import { Card } from '@ens-scan/frontend/ui/components/card';
import { DomainOwnerLink } from '@ens-scan/frontend/ui/components/domain-owner-link';

import { useQueryErrorHandling } from '@ens-scan/frontend/graphql/hooks';

import { useDomainInfoQuery } from './domain-info.generated';

gql`
  query DomainInfo($id: ID!) {
    domain(id: $id) {
      name

      parent {
        id
        name
      }

      owner
      ownerEthereumName
    }
  }
`;

export type DomainInfoProps = {
  id: string;
};

export const DomainInfo: FC<DomainInfoProps> = ({ id }) => {
  const [{ data }] = useQueryErrorHandling(
    useDomainInfoQuery({ variables: { id } })
  );

  if (!data.domain) {
    throw new Error('Unable to fetch domain');
  }

  return (
    <Card p={4}>
      <VStack spacing={2} alignItems="flex-start">
        {data.domain.name && (
          <Heading size="md" wordBreak="break-all">
            {data.domain.name}
          </Heading>
        )}

        <DomainOwnerLink
          name={data.domain.owner}
          ethereumName={data.domain.ownerEthereumName}
        />

        {data.domain.parent && (
          <Text>
            <Text as="span" fontWeight="bold">
              Parent domain:{' '}
            </Text>
            <Flex
              display="inline-flex"
              align="center"
              as={RouterLink}
              to={`/domain/${data.domain.parent.id}`}
            >
              {data.domain?.parent.name ?? data.domain?.parent.id}
            </Flex>
          </Text>
        )}
      </VStack>
    </Card>
  );
};
