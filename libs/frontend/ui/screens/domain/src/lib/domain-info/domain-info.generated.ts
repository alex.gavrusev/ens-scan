import * as Types from '@ens-scan/frontend/graphql/codegen/types';

import gql from 'graphql-tag';
import * as Urql from 'urql';
import { graphql, ResponseResolver, GraphQLRequest, GraphQLContext } from 'msw';
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type DomainInfoQueryVariables = Types.Exact<{
  id: Types.Scalars['ID'];
}>;

export type DomainInfoQuery = {
  __typename?: 'Query';
  domain?: {
    __typename?: 'Domain';
    name?: string | null;
    owner: string;
    ownerEthereumName?: string | null;
    parent?: { __typename?: 'Domain'; id: string; name?: string | null } | null;
  } | null;
};

export const DomainInfoDocument = gql`
  query DomainInfo($id: ID!) {
    domain(id: $id) {
      name
      parent {
        id
        name
      }
      owner
      ownerEthereumName
    }
  }
`;

export function useDomainInfoQuery(
  options: Omit<Urql.UseQueryArgs<DomainInfoQueryVariables>, 'query'>
) {
  return Urql.useQuery<DomainInfoQuery, DomainInfoQueryVariables>({
    query: DomainInfoDocument,
    ...options,
  });
}

/**
 * @param resolver a function that accepts a captured request and may return a mocked response.
 * @see https://mswjs.io/docs/basics/response-resolver
 * @example
 * mockDomainInfoQuery((req, res, ctx) => {
 *   const { id } = req.variables;
 *   return res(
 *     ctx.data({ domain })
 *   )
 * })
 */
export const mockDomainInfoQuery = (
  resolver: ResponseResolver<
    GraphQLRequest<DomainInfoQueryVariables>,
    GraphQLContext<DomainInfoQuery>,
    any
  >
) =>
  graphql.query<DomainInfoQuery, DomainInfoQueryVariables>(
    'DomainInfo',
    resolver
  );
