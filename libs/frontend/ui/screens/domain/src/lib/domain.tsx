import { VStack } from '@chakra-ui/react';
import { FC, Suspense } from 'react';
import { useParams } from 'react-router-dom';

import { DomainInfo } from './domain-info';
import { DomainTransfers, DomainTransfersSkeleton } from './domain-transfers';

export const DomainScreen: FC = () => {
  const { id } = useParams();

  if (!id) {
    throw new Error();
  }

  return (
    <VStack spacing={6} my={4} mx="auto" maxW="container.md" align="flex-start">
      <DomainInfo id={id} />

      <Suspense fallback={<DomainTransfersSkeleton />}>
        <DomainTransfers id={id} />
      </Suspense>
    </VStack>
  );
};
