import { FC } from 'react';
import { gql } from 'urql';
import { VStack, Text } from '@chakra-ui/react';

import { TimestampDisplay } from '@ens-scan/frontend/ui/components/timestamp-display';
import { Card } from '@ens-scan/frontend/ui/components/card';
import { DomainOwnerLink } from '@ens-scan/frontend/ui/components/domain-owner-link';

import { DomainTransferFragment } from './domain-transfer.generated';

gql`
  fragment DomainTransfer on Transfer {
    eventType
    owner
    ownerEthereumName
    createdAtTimestamp
  }
`;

export type DomainTransferProps = {
  transfer: DomainTransferFragment;
};

export const DomainTransfer: FC<DomainTransferProps> = ({ transfer }) => (
  <Card p={4}>
    <VStack align="flex-start">
      <TimestampDisplay timestamp={transfer.createdAtTimestamp} includeYear />

      <DomainOwnerLink
        name={transfer.owner}
        ethereumName={transfer.ownerEthereumName}
      />

      <Text>Transfer event type: {transfer.eventType}</Text>
    </VStack>
  </Card>
);
