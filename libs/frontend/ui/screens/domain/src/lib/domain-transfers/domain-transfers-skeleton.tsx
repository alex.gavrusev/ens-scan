import { FC } from 'react';
import { VStack } from '@chakra-ui/react';

import { CardSkeleton } from '@ens-scan/frontend/ui/components/card';

import { DomainTransfersHeading } from './domain-transfers-heading';

export const DomainTransfersSkeleton: FC = () => (
  <VStack spacing={4} w="full" align="stretch">
    <DomainTransfersHeading />

    {Array(5)
      .fill(null)
      .map(() => (
        <CardSkeleton h={122} />
      ))}
  </VStack>
);
