import * as Types from '@ens-scan/frontend/graphql/codegen/types';

import gql from 'graphql-tag';
import { DomainTransferFragmentDoc } from './domain-transfer/domain-transfer.generated';
import * as Urql from 'urql';
import { graphql, ResponseResolver, GraphQLRequest, GraphQLContext } from 'msw';
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type DomainTransfersQueryVariables = Types.Exact<{
  id: Types.Scalars['ID'];
}>;

export type DomainTransfersQuery = {
  __typename?: 'Query';
  domain?: {
    __typename?: 'Domain';
    transfers: Array<{
      __typename?: 'Transfer';
      id: string;
      eventType: Types.TransferEventType;
      owner: string;
      ownerEthereumName?: string | null;
      createdAtTimestamp: string;
    }>;
  } | null;
};

export const DomainTransfersDocument = gql`
  query DomainTransfers($id: ID!) {
    domain(id: $id) {
      transfers(first: 10, orderBy: createdAtTimestamp, orderDirection: desc) {
        id
        ...DomainTransfer
      }
    }
  }
  ${DomainTransferFragmentDoc}
`;

export function useDomainTransfersQuery(
  options: Omit<Urql.UseQueryArgs<DomainTransfersQueryVariables>, 'query'>
) {
  return Urql.useQuery<DomainTransfersQuery, DomainTransfersQueryVariables>({
    query: DomainTransfersDocument,
    ...options,
  });
}

/**
 * @param resolver a function that accepts a captured request and may return a mocked response.
 * @see https://mswjs.io/docs/basics/response-resolver
 * @example
 * mockDomainTransfersQuery((req, res, ctx) => {
 *   const { id } = req.variables;
 *   return res(
 *     ctx.data({ domain })
 *   )
 * })
 */
export const mockDomainTransfersQuery = (
  resolver: ResponseResolver<
    GraphQLRequest<DomainTransfersQueryVariables>,
    GraphQLContext<DomainTransfersQuery>,
    any
  >
) =>
  graphql.query<DomainTransfersQuery, DomainTransfersQueryVariables>(
    'DomainTransfers',
    resolver
  );
