import * as Types from '@ens-scan/frontend/graphql/codegen/types';

import gql from 'graphql-tag';
export type DomainTransferFragment = {
  __typename?: 'Transfer';
  eventType: Types.TransferEventType;
  owner: string;
  ownerEthereumName?: string | null;
  createdAtTimestamp: string;
};

export const DomainTransferFragmentDoc = gql`
  fragment DomainTransfer on Transfer {
    eventType
    owner
    ownerEthereumName
    createdAtTimestamp
  }
`;
