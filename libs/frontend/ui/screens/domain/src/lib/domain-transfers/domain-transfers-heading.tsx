import { Heading } from '@chakra-ui/react';
import { FC } from 'react';

export const DomainTransfersHeading: FC = () => (
  <Heading size="md">Domain transfers</Heading>
);
