import { VStack } from '@chakra-ui/react';
import { FC } from 'react';
import { gql } from 'urql';

import { useQueryErrorHandling } from '@ens-scan/frontend/graphql/hooks';

import { DomainTransfer, DomainTransferFragmentDoc } from './domain-transfer';
import { DomainTransfersHeading } from './domain-transfers-heading';

import { useDomainTransfersQuery } from './domain-transfers.generated';

gql`
  ${DomainTransferFragmentDoc}

  query DomainTransfers($id: ID!) {
    domain(id: $id) {
      transfers(first: 10, orderBy: createdAtTimestamp, orderDirection: desc) {
        id
        ...DomainTransfer
      }
    }
  }
`;

export type DomainTransfersProps = {
  id: string;
};

export const DomainTransfers: FC<DomainTransfersProps> = ({ id }) => {
  const [{ data }] = useQueryErrorHandling(
    useDomainTransfersQuery({ variables: { id } })
  );

  return (
    <VStack spacing={4} w="full" align="stretch">
      <DomainTransfersHeading />

      {data?.domain?.transfers.map((transfer) => (
        <DomainTransfer key={transfer.id} transfer={transfer} />
      ))}
    </VStack>
  );
};
