# frontend-ui-screens-domain

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test frontend-ui-screens-domain` to execute the unit tests via [Jest](https://jestjs.io).
