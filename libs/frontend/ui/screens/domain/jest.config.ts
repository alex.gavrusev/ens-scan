import { getJestConfig } from '@ens-scan/config/test/web';

export default getJestConfig('frontend-ui-screens-domain');
