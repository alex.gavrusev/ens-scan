import { FC, Suspense } from 'react';
import { Heading, VStack } from '@chakra-ui/react';

import { TransferList, TransferListSkeleton } from './transfer-list';

export const RecentTransfersScreen: FC = () => {
  return (
    <VStack spacing={4} my={4} mx="auto" maxW="container.md" align="flex-start">
      <Heading>Recent transfers</Heading>

      <Suspense fallback={<TransferListSkeleton />}>
        <TransferList />
      </Suspense>
    </VStack>
  );
};
