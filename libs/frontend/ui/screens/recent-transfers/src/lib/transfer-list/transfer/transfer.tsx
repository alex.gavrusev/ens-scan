import { FC } from 'react';
import { VStack, Text, LinkBox, LinkOverlay } from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { gql } from 'urql';

import { TimestampDisplay } from '@ens-scan/frontend/ui/components/timestamp-display';
import { Card } from '@ens-scan/frontend/ui/components/card';
import { DomainOwnerLink } from '@ens-scan/frontend/ui/components/domain-owner-link';

import { Transfer_TransferFragment } from './transfer.generated';

gql`
  fragment Transfer_Transfer on Transfer {
    id

    eventType
    createdAtTimestamp

    owner
    ownerEthereumName

    domain {
      id
      name
    }
  }
`;

export type TransferProps = {
  transfer: Transfer_TransferFragment;
};

export const Transfer: FC<TransferProps> = ({ transfer }) => (
  <LinkBox w="full">
    <Card p={4}>
      <VStack spacing={2} align="flex-start">
        <TimestampDisplay timestamp={transfer.createdAtTimestamp} />

        <Text>
          <LinkOverlay as={Link} to={`/domain/${transfer.domain.id}`}>
            <Text as="span" fontWeight="bold">
              Domain:{' '}
            </Text>
            {transfer.domain.name ?? transfer.domain.id}
          </LinkOverlay>
        </Text>

        <DomainOwnerLink
          name={transfer.owner}
          ethereumName={transfer.ownerEthereumName}
        />

        <Text>Transfer event type: {transfer.eventType}</Text>
      </VStack>
    </Card>
  </LinkBox>
);
