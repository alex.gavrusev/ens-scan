import { FC } from 'react';
import { VStack } from '@chakra-ui/react';

import { CardSkeleton } from '@ens-scan/frontend/ui/components/card';

export const TransferListSkeleton: FC = () => (
  <VStack spacing={4} w="full" align="stretch">
    {Array(15)
      .fill(null)
      .map(() => (
        <CardSkeleton h={152} />
      ))}
  </VStack>
);
