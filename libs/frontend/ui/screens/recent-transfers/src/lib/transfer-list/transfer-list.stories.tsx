import { ComponentMeta, ComponentStory } from '@storybook/react';
import { Suspense } from 'react';

import { TransferEventType } from '@ens-scan/frontend/graphql/codegen/types';

import { TransferList } from './transfer-list';
import { TransferListSkeleton } from './transfer-list-skeleton';
import { mockTransferListQuery } from './transfer-list.generated';

export default {
  title: 'Recent transfers/Transfer list',
  component: TransferList,
} as ComponentMeta<typeof TransferList>;

const Template: ComponentStory<typeof TransferList> = (args) => (
  <Suspense fallback={<TransferListSkeleton />}>
    <TransferList />
  </Suspense>
);
Template.parameters = {
  docs: {
    source: {
      code: 'Disabled for this story, see https://github.com/storybookjs/storybook/issues/11554',
    },
  },
};

export const Default = Template.bind({});
Default.parameters = {
  ...Template.parameters,
  msw: {
    handlers: [
      mockTransferListQuery((_, res, ctx) =>
        res(
          ctx.data({
            transfers: [
              {
                id: 'id',
                eventType: TransferEventType.Transfer,
                createdAtTimestamp: '1638848683',
                owner: 'owner.eth',
                domain: {
                  id: 'domain',
                  name: 'domain.eth',
                },
              },
            ],
          })
        )
      ),
    ],
  },
};

export const Loading = Template.bind({});
Loading.parameters = {
  ...Template.parameters,
  msw: {
    handlers: [
      mockTransferListQuery((_, res, ctx) => res(ctx.delay('infinite'))),
    ],
  },
};
