import { VStack } from '@chakra-ui/react';
import { FC } from 'react';
import { gql } from 'urql';

import { useQueryErrorHandling } from '@ens-scan/frontend/graphql/hooks';

import { Transfer, Transfer_TransferFragmentDoc } from './transfer';

import { useTransferListQuery } from './transfer-list.generated';

gql`
  ${Transfer_TransferFragmentDoc}

  query TransferList {
    transfers(first: 15, orderBy: createdAtTimestamp, orderDirection: desc) {
      ...Transfer_Transfer
    }
  }
`;

export const TransferList: FC = () => {
  const [{ data }] = useQueryErrorHandling(useTransferListQuery());

  return (
    <VStack spacing={4}>
      {data.transfers.map((transfer) => (
        <Transfer key={transfer.id} transfer={transfer} />
      ))}
    </VStack>
  );
};
