import * as Types from '@ens-scan/frontend/graphql/codegen/types';

import gql from 'graphql-tag';
import { Transfer_TransferFragmentDoc } from './transfer/transfer.generated';
import * as Urql from 'urql';
import { graphql, ResponseResolver, GraphQLRequest, GraphQLContext } from 'msw';
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type TransferListQueryVariables = Types.Exact<{ [key: string]: never }>;

export type TransferListQuery = {
  __typename?: 'Query';
  transfers: Array<{
    __typename?: 'Transfer';
    id: string;
    eventType: Types.TransferEventType;
    createdAtTimestamp: string;
    owner: string;
    ownerEthereumName?: string | null;
    domain: { __typename?: 'Domain'; id: string; name?: string | null };
  }>;
};

export const TransferListDocument = gql`
  query TransferList {
    transfers(first: 15, orderBy: createdAtTimestamp, orderDirection: desc) {
      ...Transfer_Transfer
    }
  }
  ${Transfer_TransferFragmentDoc}
`;

export function useTransferListQuery(
  options?: Omit<Urql.UseQueryArgs<TransferListQueryVariables>, 'query'>
) {
  return Urql.useQuery<TransferListQuery, TransferListQueryVariables>({
    query: TransferListDocument,
    ...options,
  });
}

/**
 * @param resolver a function that accepts a captured request and may return a mocked response.
 * @see https://mswjs.io/docs/basics/response-resolver
 * @example
 * mockTransferListQuery((req, res, ctx) => {
 *   return res(
 *     ctx.data({ transfers })
 *   )
 * })
 */
export const mockTransferListQuery = (
  resolver: ResponseResolver<
    GraphQLRequest<TransferListQueryVariables>,
    GraphQLContext<TransferListQuery>,
    any
  >
) =>
  graphql.query<TransferListQuery, TransferListQueryVariables>(
    'TransferList',
    resolver
  );
