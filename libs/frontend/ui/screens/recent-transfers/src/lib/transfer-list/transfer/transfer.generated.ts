import * as Types from '@ens-scan/frontend/graphql/codegen/types';

import gql from 'graphql-tag';
export type Transfer_TransferFragment = {
  __typename?: 'Transfer';
  id: string;
  eventType: Types.TransferEventType;
  createdAtTimestamp: string;
  owner: string;
  ownerEthereumName?: string | null;
  domain: { __typename?: 'Domain'; id: string; name?: string | null };
};

export const Transfer_TransferFragmentDoc = gql`
  fragment Transfer_Transfer on Transfer {
    id
    eventType
    createdAtTimestamp
    owner
    ownerEthereumName
    domain {
      id
      name
    }
  }
`;
