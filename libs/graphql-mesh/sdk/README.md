# graphql-mesh-sdk

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build graphql-mesh-sdk` to build the library.

### Build directory notes

`.mesh` is copied, not moved, bc graphql-mesh expects it to be present in subsequent runs
It's copied to `mesh`, because swc fails to pick-up dirs prefixed with `.`
