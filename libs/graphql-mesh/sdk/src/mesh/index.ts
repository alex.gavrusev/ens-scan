// @ts-nocheck
import {
  GraphQLResolveInfo,
  SelectionSetNode,
  FieldNode,
  GraphQLScalarType,
  GraphQLScalarTypeConfig,
} from 'graphql';
import type { GetMeshOptions } from '@graphql-mesh/runtime';
import type { YamlConfig } from '@graphql-mesh/types';
import { PubSub } from '@graphql-mesh/utils';
import { DefaultLogger } from '@graphql-mesh/utils';
import MeshCache from '@graphql-mesh/cache-localforage';
import { fetch as fetchFn } from '@whatwg-node/fetch';

import { MeshResolvedSource } from '@graphql-mesh/runtime';
import { MeshTransform, MeshPlugin } from '@graphql-mesh/types';
import GraphqlHandler from '@graphql-mesh/graphql';
import { parse } from 'graphql';
import { resolveAdditionalResolvers } from '@graphql-mesh/utils';
import StitchingMerger from '@graphql-mesh/merger-stitching';
import { printWithCache } from '@graphql-mesh/utils';
import { createMeshHTTPHandler, MeshHTTPHandler } from '@graphql-mesh/http';
import {
  getMesh,
  ExecuteMeshFn,
  SubscribeMeshFn,
  MeshContext as BaseMeshContext,
  MeshInstance,
} from '@graphql-mesh/runtime';
import { MeshStore, FsStoreStorageAdapter } from '@graphql-mesh/store';
import { path as pathModule } from '@graphql-mesh/cross-helpers';
import { ImportFn } from '@graphql-mesh/types';
import type { BackendTypes } from './sources/backend/types';
import type { SubgraphTypes } from './sources/subgraph/types';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type RequireFields<T, K extends keyof T> = Omit<T, K> & {
  [P in K]-?: NonNullable<T[P]>;
};

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  BigDecimal: any;
  BigInt: any;
  Bytes: any;
  ResolveToSourceArgs: any;
};

export type Query = {
  domain?: Maybe<Domain>;
  domains: Array<Domain>;
  transfer?: Maybe<Transfer>;
  transfers: Array<Transfer>;
  /** Access to subgraph metadata */
  _meta?: Maybe<_Meta_>;
  ethereumName?: Maybe<Scalars['String']>;
};

export type QuerydomainArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};

export type QuerydomainsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Domain_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Domain_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};

export type QuerytransferArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};

export type QuerytransfersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Transfer_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Transfer_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};

export type Query_metaArgs = {
  block?: InputMaybe<Block_height>;
};

export type QueryethereumNameArgs = {
  address: Scalars['String'];
};

export type Subscription = {
  domain?: Maybe<Domain>;
  domains: Array<Domain>;
  transfer?: Maybe<Transfer>;
  transfers: Array<Transfer>;
  /** Access to subgraph metadata */
  _meta?: Maybe<_Meta_>;
};

export type SubscriptiondomainArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};

export type SubscriptiondomainsArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Domain_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Domain_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};

export type SubscriptiontransferArgs = {
  id: Scalars['ID'];
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};

export type SubscriptiontransfersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Transfer_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Transfer_filter>;
  block?: InputMaybe<Block_height>;
  subgraphError?: _SubgraphErrorPolicy_;
};

export type Subscription_metaArgs = {
  block?: InputMaybe<Block_height>;
};

export type BlockChangedFilter = {
  number_gte: Scalars['Int'];
};

export type Block_height = {
  hash?: InputMaybe<Scalars['Bytes']>;
  number?: InputMaybe<Scalars['Int']>;
  number_gte?: InputMaybe<Scalars['Int']>;
};

export type Domain = {
  /** The ID of Domain */
  id: Scalars['ID'];
  /** The text representation of the Domain name */
  name?: Maybe<Scalars['String']>;
  /** The Address of the owner of the Domain */
  owner: Scalars['String'];
  /** The parent Domain of the Domain */
  parent?: Maybe<Domain>;
  /** The timestamp of the block the Domain was created in */
  createdAtTimestamp: Scalars['BigInt'];
  /** The Transfers of this Domain */
  transfers: Array<Transfer>;
  /** The main ethereum name of the `owner` address */
  ownerEthereumName?: Maybe<Scalars['String']>;
};

export type DomaintransfersArgs = {
  skip?: InputMaybe<Scalars['Int']>;
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Transfer_orderBy>;
  orderDirection?: InputMaybe<OrderDirection>;
  where?: InputMaybe<Transfer_filter>;
};

export type Domain_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  name?: InputMaybe<Scalars['String']>;
  name_not?: InputMaybe<Scalars['String']>;
  name_gt?: InputMaybe<Scalars['String']>;
  name_lt?: InputMaybe<Scalars['String']>;
  name_gte?: InputMaybe<Scalars['String']>;
  name_lte?: InputMaybe<Scalars['String']>;
  name_in?: InputMaybe<Array<Scalars['String']>>;
  name_not_in?: InputMaybe<Array<Scalars['String']>>;
  name_contains?: InputMaybe<Scalars['String']>;
  name_contains_nocase?: InputMaybe<Scalars['String']>;
  name_not_contains?: InputMaybe<Scalars['String']>;
  name_not_contains_nocase?: InputMaybe<Scalars['String']>;
  name_starts_with?: InputMaybe<Scalars['String']>;
  name_starts_with_nocase?: InputMaybe<Scalars['String']>;
  name_not_starts_with?: InputMaybe<Scalars['String']>;
  name_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  name_ends_with?: InputMaybe<Scalars['String']>;
  name_ends_with_nocase?: InputMaybe<Scalars['String']>;
  name_not_ends_with?: InputMaybe<Scalars['String']>;
  name_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  owner?: InputMaybe<Scalars['String']>;
  owner_not?: InputMaybe<Scalars['String']>;
  owner_gt?: InputMaybe<Scalars['String']>;
  owner_lt?: InputMaybe<Scalars['String']>;
  owner_gte?: InputMaybe<Scalars['String']>;
  owner_lte?: InputMaybe<Scalars['String']>;
  owner_in?: InputMaybe<Array<Scalars['String']>>;
  owner_not_in?: InputMaybe<Array<Scalars['String']>>;
  owner_contains?: InputMaybe<Scalars['String']>;
  owner_contains_nocase?: InputMaybe<Scalars['String']>;
  owner_not_contains?: InputMaybe<Scalars['String']>;
  owner_not_contains_nocase?: InputMaybe<Scalars['String']>;
  owner_starts_with?: InputMaybe<Scalars['String']>;
  owner_starts_with_nocase?: InputMaybe<Scalars['String']>;
  owner_not_starts_with?: InputMaybe<Scalars['String']>;
  owner_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  owner_ends_with?: InputMaybe<Scalars['String']>;
  owner_ends_with_nocase?: InputMaybe<Scalars['String']>;
  owner_not_ends_with?: InputMaybe<Scalars['String']>;
  owner_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  parent?: InputMaybe<Scalars['String']>;
  parent_not?: InputMaybe<Scalars['String']>;
  parent_gt?: InputMaybe<Scalars['String']>;
  parent_lt?: InputMaybe<Scalars['String']>;
  parent_gte?: InputMaybe<Scalars['String']>;
  parent_lte?: InputMaybe<Scalars['String']>;
  parent_in?: InputMaybe<Array<Scalars['String']>>;
  parent_not_in?: InputMaybe<Array<Scalars['String']>>;
  parent_contains?: InputMaybe<Scalars['String']>;
  parent_contains_nocase?: InputMaybe<Scalars['String']>;
  parent_not_contains?: InputMaybe<Scalars['String']>;
  parent_not_contains_nocase?: InputMaybe<Scalars['String']>;
  parent_starts_with?: InputMaybe<Scalars['String']>;
  parent_starts_with_nocase?: InputMaybe<Scalars['String']>;
  parent_not_starts_with?: InputMaybe<Scalars['String']>;
  parent_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  parent_ends_with?: InputMaybe<Scalars['String']>;
  parent_ends_with_nocase?: InputMaybe<Scalars['String']>;
  parent_not_ends_with?: InputMaybe<Scalars['String']>;
  parent_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  parent_?: InputMaybe<Domain_filter>;
  createdAtTimestamp?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_not?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_gt?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_lt?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_gte?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_lte?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_in?: InputMaybe<Array<Scalars['BigInt']>>;
  createdAtTimestamp_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  transfers_?: InputMaybe<Transfer_filter>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Domain_orderBy =
  | 'id'
  | 'name'
  | 'owner'
  | 'parent'
  | 'createdAtTimestamp'
  | 'transfers';

/** Defines the order direction, either ascending or descending */
export type OrderDirection = 'asc' | 'desc';

export type Transfer = {
  /** The ID of Transfer */
  id: Scalars['ID'];
  /** The Domain being transferred */
  domain: Domain;
  /** The new owner of the Domain */
  owner: Scalars['String'];
  /** which transfer event was handled */
  eventType: TransferEventType;
  /** The timestamp of the block the Transfer was created in */
  createdAtTimestamp: Scalars['BigInt'];
  /** The main ethereum name of the `owner` address */
  ownerEthereumName?: Maybe<Scalars['String']>;
};

export type TransferEventType = 'NewOwner' | 'Transfer';

export type Transfer_filter = {
  id?: InputMaybe<Scalars['ID']>;
  id_not?: InputMaybe<Scalars['ID']>;
  id_gt?: InputMaybe<Scalars['ID']>;
  id_lt?: InputMaybe<Scalars['ID']>;
  id_gte?: InputMaybe<Scalars['ID']>;
  id_lte?: InputMaybe<Scalars['ID']>;
  id_in?: InputMaybe<Array<Scalars['ID']>>;
  id_not_in?: InputMaybe<Array<Scalars['ID']>>;
  domain?: InputMaybe<Scalars['String']>;
  domain_not?: InputMaybe<Scalars['String']>;
  domain_gt?: InputMaybe<Scalars['String']>;
  domain_lt?: InputMaybe<Scalars['String']>;
  domain_gte?: InputMaybe<Scalars['String']>;
  domain_lte?: InputMaybe<Scalars['String']>;
  domain_in?: InputMaybe<Array<Scalars['String']>>;
  domain_not_in?: InputMaybe<Array<Scalars['String']>>;
  domain_contains?: InputMaybe<Scalars['String']>;
  domain_contains_nocase?: InputMaybe<Scalars['String']>;
  domain_not_contains?: InputMaybe<Scalars['String']>;
  domain_not_contains_nocase?: InputMaybe<Scalars['String']>;
  domain_starts_with?: InputMaybe<Scalars['String']>;
  domain_starts_with_nocase?: InputMaybe<Scalars['String']>;
  domain_not_starts_with?: InputMaybe<Scalars['String']>;
  domain_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  domain_ends_with?: InputMaybe<Scalars['String']>;
  domain_ends_with_nocase?: InputMaybe<Scalars['String']>;
  domain_not_ends_with?: InputMaybe<Scalars['String']>;
  domain_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  domain_?: InputMaybe<Domain_filter>;
  owner?: InputMaybe<Scalars['String']>;
  owner_not?: InputMaybe<Scalars['String']>;
  owner_gt?: InputMaybe<Scalars['String']>;
  owner_lt?: InputMaybe<Scalars['String']>;
  owner_gte?: InputMaybe<Scalars['String']>;
  owner_lte?: InputMaybe<Scalars['String']>;
  owner_in?: InputMaybe<Array<Scalars['String']>>;
  owner_not_in?: InputMaybe<Array<Scalars['String']>>;
  owner_contains?: InputMaybe<Scalars['String']>;
  owner_contains_nocase?: InputMaybe<Scalars['String']>;
  owner_not_contains?: InputMaybe<Scalars['String']>;
  owner_not_contains_nocase?: InputMaybe<Scalars['String']>;
  owner_starts_with?: InputMaybe<Scalars['String']>;
  owner_starts_with_nocase?: InputMaybe<Scalars['String']>;
  owner_not_starts_with?: InputMaybe<Scalars['String']>;
  owner_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
  owner_ends_with?: InputMaybe<Scalars['String']>;
  owner_ends_with_nocase?: InputMaybe<Scalars['String']>;
  owner_not_ends_with?: InputMaybe<Scalars['String']>;
  owner_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
  eventType?: InputMaybe<TransferEventType>;
  eventType_not?: InputMaybe<TransferEventType>;
  eventType_in?: InputMaybe<Array<TransferEventType>>;
  eventType_not_in?: InputMaybe<Array<TransferEventType>>;
  createdAtTimestamp?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_not?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_gt?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_lt?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_gte?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_lte?: InputMaybe<Scalars['BigInt']>;
  createdAtTimestamp_in?: InputMaybe<Array<Scalars['BigInt']>>;
  createdAtTimestamp_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
  /** Filter for the block changed event. */
  _change_block?: InputMaybe<BlockChangedFilter>;
};

export type Transfer_orderBy =
  | 'id'
  | 'domain'
  | 'owner'
  | 'eventType'
  | 'createdAtTimestamp';

export type _Block_ = {
  /** The hash of the block */
  hash?: Maybe<Scalars['Bytes']>;
  /** The block number */
  number: Scalars['Int'];
  /** Integer representation of the timestamp stored in blocks for the chain */
  timestamp?: Maybe<Scalars['Int']>;
};

/** The type for the top-level _meta field */
export type _Meta_ = {
  /**
   * Information about a specific subgraph block. The hash of the block
   * will be null if the _meta field has a block constraint that asks for
   * a block number. It will be filled if the _meta field has no block constraint
   * and therefore asks for the latest  block
   *
   */
  block: _Block_;
  /** The deployment ID */
  deployment: Scalars['String'];
  /** If `true`, the subgraph encountered indexing errors at some past block */
  hasIndexingErrors: Scalars['Boolean'];
};

export type _SubgraphErrorPolicy_ =
  /** Data will be returned even if the subgraph has indexing errors */
  | 'allow'
  /** If the subgraph has indexing errors, data will be omitted. The default. */
  | 'deny';

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;

export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string | ((fieldNode: FieldNode) => SelectionSetNode);
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> =
  | LegacyStitchingResolver<TResult, TParent, TContext, TArgs>
  | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | ResolverWithResolve<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<
  TResult,
  TKey extends string,
  TParent,
  TContext,
  TArgs
> {
  subscribe: SubscriptionSubscribeFn<
    { [key in TKey]: TResult },
    TParent,
    TContext,
    TArgs
  >;
  resolve?: SubscriptionResolveFn<
    TResult,
    { [key in TKey]: TResult },
    TContext,
    TArgs
  >;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<
  TResult,
  TKey extends string,
  TParent,
  TContext,
  TArgs
> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<
  TResult,
  TKey extends string,
  TParent = {},
  TContext = {},
  TArgs = {}
> =
  | ((
      ...args: any[]
    ) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (
  obj: T,
  context: TContext,
  info: GraphQLResolveInfo
) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<
  TResult = {},
  TParent = {},
  TContext = {},
  TArgs = {}
> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Query: ResolverTypeWrapper<{}>;
  Subscription: ResolverTypeWrapper<{}>;
  BigDecimal: ResolverTypeWrapper<Scalars['BigDecimal']>;
  BigInt: ResolverTypeWrapper<Scalars['BigInt']>;
  BlockChangedFilter: BlockChangedFilter;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  Block_height: Block_height;
  Bytes: ResolverTypeWrapper<Scalars['Bytes']>;
  Domain: ResolverTypeWrapper<Domain>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Domain_filter: Domain_filter;
  Domain_orderBy: Domain_orderBy;
  OrderDirection: OrderDirection;
  Transfer: ResolverTypeWrapper<Transfer>;
  TransferEventType: TransferEventType;
  Transfer_filter: Transfer_filter;
  Transfer_orderBy: Transfer_orderBy;
  _Block_: ResolverTypeWrapper<_Block_>;
  _Meta_: ResolverTypeWrapper<_Meta_>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  _SubgraphErrorPolicy_: _SubgraphErrorPolicy_;
  ResolveToSourceArgs: ResolverTypeWrapper<Scalars['ResolveToSourceArgs']>;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Query: {};
  Subscription: {};
  BigDecimal: Scalars['BigDecimal'];
  BigInt: Scalars['BigInt'];
  BlockChangedFilter: BlockChangedFilter;
  Int: Scalars['Int'];
  Block_height: Block_height;
  Bytes: Scalars['Bytes'];
  Domain: Domain;
  ID: Scalars['ID'];
  String: Scalars['String'];
  Domain_filter: Domain_filter;
  Transfer: Transfer;
  Transfer_filter: Transfer_filter;
  _Block_: _Block_;
  _Meta_: _Meta_;
  Boolean: Scalars['Boolean'];
  ResolveToSourceArgs: Scalars['ResolveToSourceArgs'];
}>;

export type resolveToDirectiveArgs = {
  requiredSelectionSet?: Maybe<Scalars['String']>;
  sourceName: Scalars['String'];
  sourceTypeName: Scalars['String'];
  sourceFieldName: Scalars['String'];
  sourceSelectionSet?: Maybe<Scalars['String']>;
  sourceArgs?: Maybe<Scalars['ResolveToSourceArgs']>;
  keyField?: Maybe<Scalars['String']>;
  keysArg?: Maybe<Scalars['String']>;
  pubsubTopic?: Maybe<Scalars['String']>;
  filterBy?: Maybe<Scalars['String']>;
  additionalArgs?: Maybe<Scalars['ResolveToSourceArgs']>;
  result?: Maybe<Scalars['String']>;
  resultType?: Maybe<Scalars['String']>;
};

export type resolveToDirectiveResolver<
  Result,
  Parent,
  ContextType = MeshContext,
  Args = resolveToDirectiveArgs
> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type QueryResolvers<
  ContextType = MeshContext,
  ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']
> = ResolversObject<{
  domain?: Resolver<
    Maybe<ResolversTypes['Domain']>,
    ParentType,
    ContextType,
    RequireFields<QuerydomainArgs, 'id' | 'subgraphError'>
  >;
  domains?: Resolver<
    Array<ResolversTypes['Domain']>,
    ParentType,
    ContextType,
    RequireFields<QuerydomainsArgs, 'skip' | 'first' | 'subgraphError'>
  >;
  transfer?: Resolver<
    Maybe<ResolversTypes['Transfer']>,
    ParentType,
    ContextType,
    RequireFields<QuerytransferArgs, 'id' | 'subgraphError'>
  >;
  transfers?: Resolver<
    Array<ResolversTypes['Transfer']>,
    ParentType,
    ContextType,
    RequireFields<QuerytransfersArgs, 'skip' | 'first' | 'subgraphError'>
  >;
  _meta?: Resolver<
    Maybe<ResolversTypes['_Meta_']>,
    ParentType,
    ContextType,
    Partial<Query_metaArgs>
  >;
  ethereumName?: Resolver<
    Maybe<ResolversTypes['String']>,
    ParentType,
    ContextType,
    RequireFields<QueryethereumNameArgs, 'address'>
  >;
}>;

export type SubscriptionResolvers<
  ContextType = MeshContext,
  ParentType extends ResolversParentTypes['Subscription'] = ResolversParentTypes['Subscription']
> = ResolversObject<{
  domain?: SubscriptionResolver<
    Maybe<ResolversTypes['Domain']>,
    'domain',
    ParentType,
    ContextType,
    RequireFields<SubscriptiondomainArgs, 'id' | 'subgraphError'>
  >;
  domains?: SubscriptionResolver<
    Array<ResolversTypes['Domain']>,
    'domains',
    ParentType,
    ContextType,
    RequireFields<SubscriptiondomainsArgs, 'skip' | 'first' | 'subgraphError'>
  >;
  transfer?: SubscriptionResolver<
    Maybe<ResolversTypes['Transfer']>,
    'transfer',
    ParentType,
    ContextType,
    RequireFields<SubscriptiontransferArgs, 'id' | 'subgraphError'>
  >;
  transfers?: SubscriptionResolver<
    Array<ResolversTypes['Transfer']>,
    'transfers',
    ParentType,
    ContextType,
    RequireFields<SubscriptiontransfersArgs, 'skip' | 'first' | 'subgraphError'>
  >;
  _meta?: SubscriptionResolver<
    Maybe<ResolversTypes['_Meta_']>,
    '_meta',
    ParentType,
    ContextType,
    Partial<Subscription_metaArgs>
  >;
}>;

export interface BigDecimalScalarConfig
  extends GraphQLScalarTypeConfig<ResolversTypes['BigDecimal'], any> {
  name: 'BigDecimal';
}

export interface BigIntScalarConfig
  extends GraphQLScalarTypeConfig<ResolversTypes['BigInt'], any> {
  name: 'BigInt';
}

export interface BytesScalarConfig
  extends GraphQLScalarTypeConfig<ResolversTypes['Bytes'], any> {
  name: 'Bytes';
}

export type DomainResolvers<
  ContextType = MeshContext,
  ParentType extends ResolversParentTypes['Domain'] = ResolversParentTypes['Domain']
> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  owner?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  parent?: Resolver<Maybe<ResolversTypes['Domain']>, ParentType, ContextType>;
  createdAtTimestamp?: Resolver<
    ResolversTypes['BigInt'],
    ParentType,
    ContextType
  >;
  transfers?: Resolver<
    Array<ResolversTypes['Transfer']>,
    ParentType,
    ContextType,
    RequireFields<DomaintransfersArgs, 'skip' | 'first'>
  >;
  ownerEthereumName?: Resolver<
    Maybe<ResolversTypes['String']>,
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type TransferResolvers<
  ContextType = MeshContext,
  ParentType extends ResolversParentTypes['Transfer'] = ResolversParentTypes['Transfer']
> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  domain?: Resolver<ResolversTypes['Domain'], ParentType, ContextType>;
  owner?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  eventType?: Resolver<
    ResolversTypes['TransferEventType'],
    ParentType,
    ContextType
  >;
  createdAtTimestamp?: Resolver<
    ResolversTypes['BigInt'],
    ParentType,
    ContextType
  >;
  ownerEthereumName?: Resolver<
    Maybe<ResolversTypes['String']>,
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type _Block_Resolvers<
  ContextType = MeshContext,
  ParentType extends ResolversParentTypes['_Block_'] = ResolversParentTypes['_Block_']
> = ResolversObject<{
  hash?: Resolver<Maybe<ResolversTypes['Bytes']>, ParentType, ContextType>;
  number?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  timestamp?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type _Meta_Resolvers<
  ContextType = MeshContext,
  ParentType extends ResolversParentTypes['_Meta_'] = ResolversParentTypes['_Meta_']
> = ResolversObject<{
  block?: Resolver<ResolversTypes['_Block_'], ParentType, ContextType>;
  deployment?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  hasIndexingErrors?: Resolver<
    ResolversTypes['Boolean'],
    ParentType,
    ContextType
  >;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export interface ResolveToSourceArgsScalarConfig
  extends GraphQLScalarTypeConfig<ResolversTypes['ResolveToSourceArgs'], any> {
  name: 'ResolveToSourceArgs';
}

export type Resolvers<ContextType = MeshContext> = ResolversObject<{
  Query?: QueryResolvers<ContextType>;
  Subscription?: SubscriptionResolvers<ContextType>;
  BigDecimal?: GraphQLScalarType;
  BigInt?: GraphQLScalarType;
  Bytes?: GraphQLScalarType;
  Domain?: DomainResolvers<ContextType>;
  Transfer?: TransferResolvers<ContextType>;
  _Block_?: _Block_Resolvers<ContextType>;
  _Meta_?: _Meta_Resolvers<ContextType>;
  ResolveToSourceArgs?: GraphQLScalarType;
}>;

export type DirectiveResolvers<ContextType = MeshContext> = ResolversObject<{
  resolveTo?: resolveToDirectiveResolver<any, any, ContextType>;
}>;

export type MeshContext = SubgraphTypes.Context &
  BackendTypes.Context &
  BaseMeshContext;

const baseDir = pathModule.join(
  typeof __dirname === 'string' ? __dirname : '/',
  '..'
);

const importFn: ImportFn = <T>(moduleId: string) => {
  const relativeModuleId = (
    pathModule.isAbsolute(moduleId)
      ? pathModule.relative(baseDir, moduleId)
      : moduleId
  )
    .split('\\')
    .join('/')
    .replace(baseDir + '/', '');
  switch (relativeModuleId) {
    case '.meshrc.ts':
      return import('./../.meshrc') as T;

    default:
      return Promise.reject(
        new Error(`Cannot find module '${relativeModuleId}'.`)
      );
  }
};

const rootStore = new MeshStore(
  '.mesh',
  new FsStoreStorageAdapter({
    cwd: baseDir,
    importFn,
    fileType: 'ts',
  }),
  {
    readonly: true,
    validate: false,
  }
);

export const rawServeConfig: YamlConfig.Config['serve'] = undefined as any;
export async function getMeshOptions(): Promise<GetMeshOptions> {
  const pubsub = new PubSub();
  const sourcesStore = rootStore.child('sources');
  const logger = new DefaultLogger('🕸️  Mesh');
  const cache = new (MeshCache as any)({
    ...({} as any),
    importFn,
    store: rootStore.child('cache'),
    pubsub,
    logger,
  } as any);

  const sources: MeshResolvedSource[] = [];
  const transforms: MeshTransform[] = [];
  const additionalEnvelopPlugins: MeshPlugin<any>[] = [];
  const subgraphTransforms = [];
  const backendTransforms = [];
  const subgraphHandler = new GraphqlHandler({
    name: 'subgraph',
    config: {
      endpoint:
        'https://api.thegraph.com/subgraphs/name/throwaway193984677751/gimme-a-graph-again',
      source:
        '{"kind":"Document","definitions":[{"kind":"SchemaDefinition","operationTypes":[{"kind":"OperationTypeDefinition","operation":"query","type":{"kind":"NamedType","name":{"kind":"Name","value":"Query"}}},{"kind":"OperationTypeDefinition","operation":"subscription","type":{"kind":"NamedType","name":{"kind":"Name","value":"Subscription"}}}],"directives":[]},{"kind":"DirectiveDefinition","description":{"kind":"StringValue","value":"Marks the GraphQL type as indexable entity.  Each type that should be an entity is required to be annotated with this directive."},"name":{"kind":"Name","value":"entity"},"arguments":[],"repeatable":false,"locations":[{"kind":"Name","value":"OBJECT"}]},{"kind":"DirectiveDefinition","description":{"kind":"StringValue","value":"Defined a Subgraph ID for an object type"},"name":{"kind":"Name","value":"subgraphId"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},"directives":[]}],"repeatable":false,"locations":[{"kind":"Name","value":"OBJECT"}]},{"kind":"DirectiveDefinition","description":{"kind":"StringValue","value":"creates a virtual field on the entity that may be queried but cannot be set manually through the mappings API."},"name":{"kind":"Name","value":"derivedFrom"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"field"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},"directives":[]}],"repeatable":false,"locations":[{"kind":"Name","value":"FIELD_DEFINITION"}]},{"kind":"ScalarTypeDefinition","name":{"kind":"Name","value":"BigDecimal"},"directives":[]},{"kind":"ScalarTypeDefinition","name":{"kind":"Name","value":"BigInt"},"directives":[]},{"kind":"InputObjectTypeDefinition","name":{"kind":"Name","value":"BlockChangedFilter"},"fields":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"number_gte"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}},"directives":[]}],"directives":[]},{"kind":"InputObjectTypeDefinition","name":{"kind":"Name","value":"Block_height"},"fields":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"hash"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Bytes"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"number"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"number_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"directives":[]}],"directives":[]},{"kind":"ScalarTypeDefinition","name":{"kind":"Name","value":"Bytes"},"directives":[]},{"kind":"ObjectTypeDefinition","name":{"kind":"Name","value":"Domain"},"fields":[{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The ID of Domain","block":true},"name":{"kind":"Name","value":"id"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The text representation of the Domain name","block":true},"name":{"kind":"Name","value":"name"},"arguments":[],"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The Address of the owner of the Domain","block":true},"name":{"kind":"Name","value":"owner"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The parent Domain of the Domain","block":true},"name":{"kind":"Name","value":"parent"},"arguments":[],"type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain"}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The timestamp of the block the Domain was created in","block":true},"name":{"kind":"Name","value":"createdAtTimestamp"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The Transfers of this Domain","block":true},"name":{"kind":"Name","value":"transfers"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"skip"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"first"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"100"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderBy"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer_orderBy"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderDirection"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"OrderDirection"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"where"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer_filter"}},"directives":[]}],"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer"}}}}},"directives":[]}],"interfaces":[],"directives":[]},{"kind":"InputObjectTypeDefinition","name":{"kind":"Name","value":"Domain_filter"},"fields":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_gt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_lt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_lte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_gt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_lt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_lte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_not_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_not_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_not_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_not_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_not_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"name_not_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_gt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_lt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_lte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_gt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_lt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_lte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_not_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_not_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_not_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_not_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_not_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_not_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"parent_"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain_filter"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_gt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_lt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_lte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"transfers_"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer_filter"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Filter for the block changed event.","block":true},"name":{"kind":"Name","value":"_change_block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BlockChangedFilter"}},"directives":[]}],"directives":[]},{"kind":"EnumTypeDefinition","name":{"kind":"Name","value":"Domain_orderBy"},"values":[{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"id"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"name"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"owner"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"parent"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"transfers"},"directives":[]}],"directives":[]},{"kind":"EnumTypeDefinition","description":{"kind":"StringValue","value":"Defines the order direction, either ascending or descending","block":true},"name":{"kind":"Name","value":"OrderDirection"},"values":[{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"asc"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"desc"},"directives":[]}],"directives":[]},{"kind":"ObjectTypeDefinition","name":{"kind":"Name","value":"Query"},"fields":[{"kind":"FieldDefinition","name":{"kind":"Name","value":"domain"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.","block":true},"name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.","block":true},"name":{"kind":"Name","value":"subgraphError"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"_SubgraphErrorPolicy_"}}},"defaultValue":{"kind":"EnumValue","value":"deny"},"directives":[]}],"type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain"}},"directives":[]},{"kind":"FieldDefinition","name":{"kind":"Name","value":"domains"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"skip"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"first"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"100"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderBy"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain_orderBy"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderDirection"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"OrderDirection"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"where"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain_filter"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.","block":true},"name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.","block":true},"name":{"kind":"Name","value":"subgraphError"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"_SubgraphErrorPolicy_"}}},"defaultValue":{"kind":"EnumValue","value":"deny"},"directives":[]}],"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain"}}}}},"directives":[]},{"kind":"FieldDefinition","name":{"kind":"Name","value":"transfer"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.","block":true},"name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.","block":true},"name":{"kind":"Name","value":"subgraphError"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"_SubgraphErrorPolicy_"}}},"defaultValue":{"kind":"EnumValue","value":"deny"},"directives":[]}],"type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer"}},"directives":[]},{"kind":"FieldDefinition","name":{"kind":"Name","value":"transfers"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"skip"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"first"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"100"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderBy"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer_orderBy"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderDirection"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"OrderDirection"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"where"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer_filter"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.","block":true},"name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.","block":true},"name":{"kind":"Name","value":"subgraphError"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"_SubgraphErrorPolicy_"}}},"defaultValue":{"kind":"EnumValue","value":"deny"},"directives":[]}],"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer"}}}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"Access to subgraph metadata","block":true},"name":{"kind":"Name","value":"_meta"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]}],"type":{"kind":"NamedType","name":{"kind":"Name","value":"_Meta_"}},"directives":[]}],"interfaces":[],"directives":[]},{"kind":"ObjectTypeDefinition","name":{"kind":"Name","value":"Subscription"},"fields":[{"kind":"FieldDefinition","name":{"kind":"Name","value":"domain"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.","block":true},"name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.","block":true},"name":{"kind":"Name","value":"subgraphError"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"_SubgraphErrorPolicy_"}}},"defaultValue":{"kind":"EnumValue","value":"deny"},"directives":[]}],"type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain"}},"directives":[]},{"kind":"FieldDefinition","name":{"kind":"Name","value":"domains"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"skip"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"first"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"100"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderBy"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain_orderBy"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderDirection"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"OrderDirection"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"where"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain_filter"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.","block":true},"name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.","block":true},"name":{"kind":"Name","value":"subgraphError"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"_SubgraphErrorPolicy_"}}},"defaultValue":{"kind":"EnumValue","value":"deny"},"directives":[]}],"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain"}}}}},"directives":[]},{"kind":"FieldDefinition","name":{"kind":"Name","value":"transfer"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.","block":true},"name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.","block":true},"name":{"kind":"Name","value":"subgraphError"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"_SubgraphErrorPolicy_"}}},"defaultValue":{"kind":"EnumValue","value":"deny"},"directives":[]}],"type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer"}},"directives":[]},{"kind":"FieldDefinition","name":{"kind":"Name","value":"transfers"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"skip"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"0"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"first"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"defaultValue":{"kind":"IntValue","value":"100"},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderBy"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer_orderBy"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"orderDirection"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"OrderDirection"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"where"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer_filter"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"The block at which the query should be executed. Can either be a `{ hash: Bytes }` value containing a block hash, a `{ number: Int }` containing the block number, or a `{ number_gte: Int }` containing the minimum block number. In the case of `number_gte`, the query will be executed on the latest block only if the subgraph has progressed to or past the minimum block number. Defaults to the latest block when omitted.","block":true},"name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Set to `allow` to receive data even if the subgraph has skipped over errors while syncing.","block":true},"name":{"kind":"Name","value":"subgraphError"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"_SubgraphErrorPolicy_"}}},"defaultValue":{"kind":"EnumValue","value":"deny"},"directives":[]}],"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Transfer"}}}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"Access to subgraph metadata","block":true},"name":{"kind":"Name","value":"_meta"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Block_height"}},"directives":[]}],"type":{"kind":"NamedType","name":{"kind":"Name","value":"_Meta_"}},"directives":[]}],"interfaces":[],"directives":[]},{"kind":"ObjectTypeDefinition","name":{"kind":"Name","value":"Transfer"},"fields":[{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The ID of Transfer","block":true},"name":{"kind":"Name","value":"id"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The Domain being transferred","block":true},"name":{"kind":"Name","value":"domain"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The new owner of the Domain","block":true},"name":{"kind":"Name","value":"owner"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"which transfer event was handled","block":true},"name":{"kind":"Name","value":"eventType"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"TransferEventType"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The timestamp of the block the Transfer was created in","block":true},"name":{"kind":"Name","value":"createdAtTimestamp"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}}},"directives":[]}],"interfaces":[],"directives":[]},{"kind":"EnumTypeDefinition","name":{"kind":"Name","value":"TransferEventType"},"values":[{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"NewOwner"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"Transfer"},"directives":[]}],"directives":[]},{"kind":"InputObjectTypeDefinition","name":{"kind":"Name","value":"Transfer_filter"},"fields":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_gt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_lt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_lte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"id_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_gt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_lt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_lte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_not_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_not_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_not_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_not_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_not_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_not_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"domain_"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Domain_filter"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_gt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_lt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_lte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_contains"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_contains_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_starts_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_starts_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_ends_with"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"owner_not_ends_with_nocase"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"eventType"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"TransferEventType"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"eventType_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"TransferEventType"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"eventType_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"TransferEventType"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"eventType_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"TransferEventType"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_not"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_gt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_lt"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_gte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_lte"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}}}},"directives":[]},{"kind":"InputValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp_not_in"},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"BigInt"}}}},"directives":[]},{"kind":"InputValueDefinition","description":{"kind":"StringValue","value":"Filter for the block changed event.","block":true},"name":{"kind":"Name","value":"_change_block"},"type":{"kind":"NamedType","name":{"kind":"Name","value":"BlockChangedFilter"}},"directives":[]}],"directives":[]},{"kind":"EnumTypeDefinition","name":{"kind":"Name","value":"Transfer_orderBy"},"values":[{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"id"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"domain"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"owner"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"eventType"},"directives":[]},{"kind":"EnumValueDefinition","name":{"kind":"Name","value":"createdAtTimestamp"},"directives":[]}],"directives":[]},{"kind":"ObjectTypeDefinition","name":{"kind":"Name","value":"_Block_"},"fields":[{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The hash of the block","block":true},"name":{"kind":"Name","value":"hash"},"arguments":[],"type":{"kind":"NamedType","name":{"kind":"Name","value":"Bytes"}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The block number","block":true},"name":{"kind":"Name","value":"number"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"Integer representation of the timestamp stored in blocks for the chain","block":true},"name":{"kind":"Name","value":"timestamp"},"arguments":[],"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}},"directives":[]}],"interfaces":[],"directives":[]},{"kind":"ObjectTypeDefinition","description":{"kind":"StringValue","value":"The type for the top-level _meta field","block":true},"name":{"kind":"Name","value":"_Meta_"},"fields":[{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"Information about a specific subgraph block. The hash of the block\\nwill be null if the _meta field has a block constraint that asks for\\na block number. It will be filled if the _meta field has no block constraint\\nand therefore asks for the latest  block\\n","block":true},"name":{"kind":"Name","value":"block"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"_Block_"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"The deployment ID","block":true},"name":{"kind":"Name","value":"deployment"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},"directives":[]},{"kind":"FieldDefinition","description":{"kind":"StringValue","value":"If `true`, the subgraph encountered indexing errors at some past block","block":true},"name":{"kind":"Name","value":"hasIndexingErrors"},"arguments":[],"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Boolean"}}},"directives":[]}],"interfaces":[],"directives":[]},{"kind":"EnumTypeDefinition","name":{"kind":"Name","value":"_SubgraphErrorPolicy_"},"values":[{"kind":"EnumValueDefinition","description":{"kind":"StringValue","value":"Data will be returned even if the subgraph has indexing errors","block":true},"name":{"kind":"Name","value":"allow"},"directives":[]},{"kind":"EnumValueDefinition","description":{"kind":"StringValue","value":"If the subgraph has indexing errors, data will be omitted. The default.","block":true},"name":{"kind":"Name","value":"deny"},"directives":[]}],"directives":[]}]}',
    },
    baseDir,
    cache,
    pubsub,
    store: sourcesStore.child('subgraph'),
    logger: logger.child('subgraph'),
    importFn,
  });
  const backendHandler = new GraphqlHandler({
    name: 'backend',
    config: {
      endpoint: 'http://0.0.0.0:3333/graphql',
      source:
        '{"kind":"Document","definitions":[{"kind":"SchemaDefinition","operationTypes":[{"kind":"OperationTypeDefinition","operation":"query","type":{"kind":"NamedType","name":{"kind":"Name","value":"Query"}}}],"directives":[]},{"kind":"ObjectTypeDefinition","name":{"kind":"Name","value":"Query"},"fields":[{"kind":"FieldDefinition","name":{"kind":"Name","value":"ethereumName"},"arguments":[{"kind":"InputValueDefinition","name":{"kind":"Name","value":"address"},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},"directives":[]}],"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}},"directives":[]}],"interfaces":[],"directives":[]}]}',
    },
    baseDir,
    cache,
    pubsub,
    store: sourcesStore.child('backend'),
    logger: logger.child('backend'),
    importFn,
  });
  sources[0] = {
    name: 'subgraph',
    handler: subgraphHandler,
    transforms: subgraphTransforms,
  };
  sources[1] = {
    name: 'backend',
    handler: backendHandler,
    transforms: backendTransforms,
  };
  const additionalTypeDefs = [
    parse(
      'extend type Domain {\n  """The main ethereum name of the `owner` address"""\n  ownerEthereumName: String @resolveTo(sourceName: "backend", sourceTypeName: "Query", sourceFieldName: "ethereumName", requiredSelectionSet: "{owner}", sourceArgs: {address: "{root.owner}"})\n}\n\nextend type Transfer {\n  """The main ethereum name of the `owner` address"""\n  ownerEthereumName: String @resolveTo(sourceName: "backend", sourceTypeName: "Query", sourceFieldName: "ethereumName", requiredSelectionSet: "{owner}", sourceArgs: {address: "{root.owner}"})\n}'
    ),
  ] as any[];
  const additionalResolvers = [] as any[];
  additionalTypeDefs.unshift(
    parse(/* GraphQL */ `
      scalar ResolveToSourceArgs
      directive @resolveTo(
        requiredSelectionSet: String
        sourceName: String!
        sourceTypeName: String!
        sourceFieldName: String!
        sourceSelectionSet: String
        sourceArgs: ResolveToSourceArgs
        keyField: String
        keysArg: String
        pubsubTopic: String
        filterBy: String
        additionalArgs: ResolveToSourceArgs
        result: String
        resultType: String
      ) on FIELD_DEFINITION
    `)
  );
  const additionalResolversFromTypeDefs = await resolveAdditionalResolvers(
    baseDir,
    [
      {
        targetTypeName: 'Domain',
        targetFieldName: 'ownerEthereumName',
        sourceName: 'backend',
        sourceTypeName: 'Query',
        sourceFieldName: 'ethereumName',
        requiredSelectionSet: '{owner}',
        sourceArgs: { address: '{root.owner}' },
      },
      {
        targetTypeName: 'Transfer',
        targetFieldName: 'ownerEthereumName',
        sourceName: 'backend',
        sourceTypeName: 'Query',
        sourceFieldName: 'ethereumName',
        requiredSelectionSet: '{owner}',
        sourceArgs: { address: '{root.owner}' },
      },
    ],
    importFn,
    pubsub
  );
  additionalResolvers.push(...additionalResolversFromTypeDefs);
  const merger = new (StitchingMerger as any)({
    cache,
    pubsub,
    logger: logger.child('stitchingMerger'),
    store: rootStore.child('stitchingMerger'),
  });

  return {
    sources,
    transforms,
    additionalTypeDefs,
    additionalResolvers,
    cache,
    pubsub,
    merger,
    logger,
    additionalEnvelopPlugins,
    get documents() {
      return [];
    },
    fetchFn,
  };
}

export function createBuiltMeshHTTPHandler(): MeshHTTPHandler<MeshContext> {
  return createMeshHTTPHandler<MeshContext>({
    baseDir,
    getBuiltMesh: getBuiltMesh,
    rawServeConfig: undefined,
  });
}

let meshInstance$: Promise<MeshInstance> | undefined;

export function getBuiltMesh(): Promise<MeshInstance> {
  if (meshInstance$ == null) {
    meshInstance$ = getMeshOptions()
      .then((meshOptions) => getMesh(meshOptions))
      .then((mesh) => {
        const id = mesh.pubsub.subscribe('destroy', () => {
          meshInstance$ = undefined;
          mesh.pubsub.unsubscribe(id);
        });
        return mesh;
      });
  }
  return meshInstance$;
}

export const execute: ExecuteMeshFn = (...args) =>
  getBuiltMesh().then(({ execute }) => execute(...args));

export const subscribe: SubscribeMeshFn = (...args) =>
  getBuiltMesh().then(({ subscribe }) => subscribe(...args));
