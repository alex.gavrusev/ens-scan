// @ts-nocheck

import { InContextSdkMethod } from '@graphql-mesh/types';
import { MeshContext } from '@graphql-mesh/runtime';

export namespace BackendTypes {
  export type Maybe<T> = T | null;
  export type InputMaybe<T> = Maybe<T>;
  export type Exact<T extends { [key: string]: unknown }> = {
    [K in keyof T]: T[K];
  };
  export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]?: Maybe<T[SubKey]>;
  };
  export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]: Maybe<T[SubKey]>;
  };
  /** All built-in and custom scalars, mapped to their actual values */
  export type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
  };

  export type Query = {
    ethereumName?: Maybe<Scalars['String']>;
  };

  export type QueryethereumNameArgs = {
    address: Scalars['String'];
  };

  export type QuerySdk = {
    /** undefined **/
    ethereumName: InContextSdkMethod<
      Query['ethereumName'],
      QueryethereumNameArgs,
      MeshContext
    >;
  };

  export type MutationSdk = {};

  export type SubscriptionSdk = {};

  export type Context = {
    ['backend']: {
      Query: QuerySdk;
      Mutation: MutationSdk;
      Subscription: SubscriptionSdk;
    };
  };
}
