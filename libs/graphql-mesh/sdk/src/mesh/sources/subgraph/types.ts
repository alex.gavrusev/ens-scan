// @ts-nocheck

import { InContextSdkMethod } from '@graphql-mesh/types';
import { MeshContext } from '@graphql-mesh/runtime';

export namespace SubgraphTypes {
  export type Maybe<T> = T | null;
  export type InputMaybe<T> = Maybe<T>;
  export type Exact<T extends { [key: string]: unknown }> = {
    [K in keyof T]: T[K];
  };
  export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]?: Maybe<T[SubKey]>;
  };
  export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
    [SubKey in K]: Maybe<T[SubKey]>;
  };
  /** All built-in and custom scalars, mapped to their actual values */
  export type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
    BigDecimal: any;
    BigInt: any;
    Bytes: any;
  };

  export type BlockChangedFilter = {
    number_gte: Scalars['Int'];
  };

  export type Block_height = {
    hash?: InputMaybe<Scalars['Bytes']>;
    number?: InputMaybe<Scalars['Int']>;
    number_gte?: InputMaybe<Scalars['Int']>;
  };

  export type Domain = {
    /** The ID of Domain */
    id: Scalars['ID'];
    /** The text representation of the Domain name */
    name?: Maybe<Scalars['String']>;
    /** The Address of the owner of the Domain */
    owner: Scalars['String'];
    /** The parent Domain of the Domain */
    parent?: Maybe<Domain>;
    /** The timestamp of the block the Domain was created in */
    createdAtTimestamp: Scalars['BigInt'];
    /** The Transfers of this Domain */
    transfers: Array<Transfer>;
  };

  export type DomaintransfersArgs = {
    skip?: InputMaybe<Scalars['Int']>;
    first?: InputMaybe<Scalars['Int']>;
    orderBy?: InputMaybe<Transfer_orderBy>;
    orderDirection?: InputMaybe<OrderDirection>;
    where?: InputMaybe<Transfer_filter>;
  };

  export type Domain_filter = {
    id?: InputMaybe<Scalars['ID']>;
    id_not?: InputMaybe<Scalars['ID']>;
    id_gt?: InputMaybe<Scalars['ID']>;
    id_lt?: InputMaybe<Scalars['ID']>;
    id_gte?: InputMaybe<Scalars['ID']>;
    id_lte?: InputMaybe<Scalars['ID']>;
    id_in?: InputMaybe<Array<Scalars['ID']>>;
    id_not_in?: InputMaybe<Array<Scalars['ID']>>;
    name?: InputMaybe<Scalars['String']>;
    name_not?: InputMaybe<Scalars['String']>;
    name_gt?: InputMaybe<Scalars['String']>;
    name_lt?: InputMaybe<Scalars['String']>;
    name_gte?: InputMaybe<Scalars['String']>;
    name_lte?: InputMaybe<Scalars['String']>;
    name_in?: InputMaybe<Array<Scalars['String']>>;
    name_not_in?: InputMaybe<Array<Scalars['String']>>;
    name_contains?: InputMaybe<Scalars['String']>;
    name_contains_nocase?: InputMaybe<Scalars['String']>;
    name_not_contains?: InputMaybe<Scalars['String']>;
    name_not_contains_nocase?: InputMaybe<Scalars['String']>;
    name_starts_with?: InputMaybe<Scalars['String']>;
    name_starts_with_nocase?: InputMaybe<Scalars['String']>;
    name_not_starts_with?: InputMaybe<Scalars['String']>;
    name_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
    name_ends_with?: InputMaybe<Scalars['String']>;
    name_ends_with_nocase?: InputMaybe<Scalars['String']>;
    name_not_ends_with?: InputMaybe<Scalars['String']>;
    name_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
    owner?: InputMaybe<Scalars['String']>;
    owner_not?: InputMaybe<Scalars['String']>;
    owner_gt?: InputMaybe<Scalars['String']>;
    owner_lt?: InputMaybe<Scalars['String']>;
    owner_gte?: InputMaybe<Scalars['String']>;
    owner_lte?: InputMaybe<Scalars['String']>;
    owner_in?: InputMaybe<Array<Scalars['String']>>;
    owner_not_in?: InputMaybe<Array<Scalars['String']>>;
    owner_contains?: InputMaybe<Scalars['String']>;
    owner_contains_nocase?: InputMaybe<Scalars['String']>;
    owner_not_contains?: InputMaybe<Scalars['String']>;
    owner_not_contains_nocase?: InputMaybe<Scalars['String']>;
    owner_starts_with?: InputMaybe<Scalars['String']>;
    owner_starts_with_nocase?: InputMaybe<Scalars['String']>;
    owner_not_starts_with?: InputMaybe<Scalars['String']>;
    owner_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
    owner_ends_with?: InputMaybe<Scalars['String']>;
    owner_ends_with_nocase?: InputMaybe<Scalars['String']>;
    owner_not_ends_with?: InputMaybe<Scalars['String']>;
    owner_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
    parent?: InputMaybe<Scalars['String']>;
    parent_not?: InputMaybe<Scalars['String']>;
    parent_gt?: InputMaybe<Scalars['String']>;
    parent_lt?: InputMaybe<Scalars['String']>;
    parent_gte?: InputMaybe<Scalars['String']>;
    parent_lte?: InputMaybe<Scalars['String']>;
    parent_in?: InputMaybe<Array<Scalars['String']>>;
    parent_not_in?: InputMaybe<Array<Scalars['String']>>;
    parent_contains?: InputMaybe<Scalars['String']>;
    parent_contains_nocase?: InputMaybe<Scalars['String']>;
    parent_not_contains?: InputMaybe<Scalars['String']>;
    parent_not_contains_nocase?: InputMaybe<Scalars['String']>;
    parent_starts_with?: InputMaybe<Scalars['String']>;
    parent_starts_with_nocase?: InputMaybe<Scalars['String']>;
    parent_not_starts_with?: InputMaybe<Scalars['String']>;
    parent_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
    parent_ends_with?: InputMaybe<Scalars['String']>;
    parent_ends_with_nocase?: InputMaybe<Scalars['String']>;
    parent_not_ends_with?: InputMaybe<Scalars['String']>;
    parent_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
    parent_?: InputMaybe<Domain_filter>;
    createdAtTimestamp?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_not?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_gt?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_lt?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_gte?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_lte?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_in?: InputMaybe<Array<Scalars['BigInt']>>;
    createdAtTimestamp_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
    transfers_?: InputMaybe<Transfer_filter>;
    /** Filter for the block changed event. */
    _change_block?: InputMaybe<BlockChangedFilter>;
  };

  export type Domain_orderBy =
    | 'id'
    | 'name'
    | 'owner'
    | 'parent'
    | 'createdAtTimestamp'
    | 'transfers';

  /** Defines the order direction, either ascending or descending */
  export type OrderDirection = 'asc' | 'desc';

  export type Query = {
    domain?: Maybe<Domain>;
    domains: Array<Domain>;
    transfer?: Maybe<Transfer>;
    transfers: Array<Transfer>;
    /** Access to subgraph metadata */
    _meta?: Maybe<_Meta_>;
  };

  export type QuerydomainArgs = {
    id: Scalars['ID'];
    block?: InputMaybe<Block_height>;
    subgraphError?: _SubgraphErrorPolicy_;
  };

  export type QuerydomainsArgs = {
    skip?: InputMaybe<Scalars['Int']>;
    first?: InputMaybe<Scalars['Int']>;
    orderBy?: InputMaybe<Domain_orderBy>;
    orderDirection?: InputMaybe<OrderDirection>;
    where?: InputMaybe<Domain_filter>;
    block?: InputMaybe<Block_height>;
    subgraphError?: _SubgraphErrorPolicy_;
  };

  export type QuerytransferArgs = {
    id: Scalars['ID'];
    block?: InputMaybe<Block_height>;
    subgraphError?: _SubgraphErrorPolicy_;
  };

  export type QuerytransfersArgs = {
    skip?: InputMaybe<Scalars['Int']>;
    first?: InputMaybe<Scalars['Int']>;
    orderBy?: InputMaybe<Transfer_orderBy>;
    orderDirection?: InputMaybe<OrderDirection>;
    where?: InputMaybe<Transfer_filter>;
    block?: InputMaybe<Block_height>;
    subgraphError?: _SubgraphErrorPolicy_;
  };

  export type Query_metaArgs = {
    block?: InputMaybe<Block_height>;
  };

  export type Subscription = {
    domain?: Maybe<Domain>;
    domains: Array<Domain>;
    transfer?: Maybe<Transfer>;
    transfers: Array<Transfer>;
    /** Access to subgraph metadata */
    _meta?: Maybe<_Meta_>;
  };

  export type SubscriptiondomainArgs = {
    id: Scalars['ID'];
    block?: InputMaybe<Block_height>;
    subgraphError?: _SubgraphErrorPolicy_;
  };

  export type SubscriptiondomainsArgs = {
    skip?: InputMaybe<Scalars['Int']>;
    first?: InputMaybe<Scalars['Int']>;
    orderBy?: InputMaybe<Domain_orderBy>;
    orderDirection?: InputMaybe<OrderDirection>;
    where?: InputMaybe<Domain_filter>;
    block?: InputMaybe<Block_height>;
    subgraphError?: _SubgraphErrorPolicy_;
  };

  export type SubscriptiontransferArgs = {
    id: Scalars['ID'];
    block?: InputMaybe<Block_height>;
    subgraphError?: _SubgraphErrorPolicy_;
  };

  export type SubscriptiontransfersArgs = {
    skip?: InputMaybe<Scalars['Int']>;
    first?: InputMaybe<Scalars['Int']>;
    orderBy?: InputMaybe<Transfer_orderBy>;
    orderDirection?: InputMaybe<OrderDirection>;
    where?: InputMaybe<Transfer_filter>;
    block?: InputMaybe<Block_height>;
    subgraphError?: _SubgraphErrorPolicy_;
  };

  export type Subscription_metaArgs = {
    block?: InputMaybe<Block_height>;
  };

  export type Transfer = {
    /** The ID of Transfer */
    id: Scalars['ID'];
    /** The Domain being transferred */
    domain: Domain;
    /** The new owner of the Domain */
    owner: Scalars['String'];
    /** which transfer event was handled */
    eventType: TransferEventType;
    /** The timestamp of the block the Transfer was created in */
    createdAtTimestamp: Scalars['BigInt'];
  };

  export type TransferEventType = 'NewOwner' | 'Transfer';

  export type Transfer_filter = {
    id?: InputMaybe<Scalars['ID']>;
    id_not?: InputMaybe<Scalars['ID']>;
    id_gt?: InputMaybe<Scalars['ID']>;
    id_lt?: InputMaybe<Scalars['ID']>;
    id_gte?: InputMaybe<Scalars['ID']>;
    id_lte?: InputMaybe<Scalars['ID']>;
    id_in?: InputMaybe<Array<Scalars['ID']>>;
    id_not_in?: InputMaybe<Array<Scalars['ID']>>;
    domain?: InputMaybe<Scalars['String']>;
    domain_not?: InputMaybe<Scalars['String']>;
    domain_gt?: InputMaybe<Scalars['String']>;
    domain_lt?: InputMaybe<Scalars['String']>;
    domain_gte?: InputMaybe<Scalars['String']>;
    domain_lte?: InputMaybe<Scalars['String']>;
    domain_in?: InputMaybe<Array<Scalars['String']>>;
    domain_not_in?: InputMaybe<Array<Scalars['String']>>;
    domain_contains?: InputMaybe<Scalars['String']>;
    domain_contains_nocase?: InputMaybe<Scalars['String']>;
    domain_not_contains?: InputMaybe<Scalars['String']>;
    domain_not_contains_nocase?: InputMaybe<Scalars['String']>;
    domain_starts_with?: InputMaybe<Scalars['String']>;
    domain_starts_with_nocase?: InputMaybe<Scalars['String']>;
    domain_not_starts_with?: InputMaybe<Scalars['String']>;
    domain_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
    domain_ends_with?: InputMaybe<Scalars['String']>;
    domain_ends_with_nocase?: InputMaybe<Scalars['String']>;
    domain_not_ends_with?: InputMaybe<Scalars['String']>;
    domain_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
    domain_?: InputMaybe<Domain_filter>;
    owner?: InputMaybe<Scalars['String']>;
    owner_not?: InputMaybe<Scalars['String']>;
    owner_gt?: InputMaybe<Scalars['String']>;
    owner_lt?: InputMaybe<Scalars['String']>;
    owner_gte?: InputMaybe<Scalars['String']>;
    owner_lte?: InputMaybe<Scalars['String']>;
    owner_in?: InputMaybe<Array<Scalars['String']>>;
    owner_not_in?: InputMaybe<Array<Scalars['String']>>;
    owner_contains?: InputMaybe<Scalars['String']>;
    owner_contains_nocase?: InputMaybe<Scalars['String']>;
    owner_not_contains?: InputMaybe<Scalars['String']>;
    owner_not_contains_nocase?: InputMaybe<Scalars['String']>;
    owner_starts_with?: InputMaybe<Scalars['String']>;
    owner_starts_with_nocase?: InputMaybe<Scalars['String']>;
    owner_not_starts_with?: InputMaybe<Scalars['String']>;
    owner_not_starts_with_nocase?: InputMaybe<Scalars['String']>;
    owner_ends_with?: InputMaybe<Scalars['String']>;
    owner_ends_with_nocase?: InputMaybe<Scalars['String']>;
    owner_not_ends_with?: InputMaybe<Scalars['String']>;
    owner_not_ends_with_nocase?: InputMaybe<Scalars['String']>;
    eventType?: InputMaybe<TransferEventType>;
    eventType_not?: InputMaybe<TransferEventType>;
    eventType_in?: InputMaybe<Array<TransferEventType>>;
    eventType_not_in?: InputMaybe<Array<TransferEventType>>;
    createdAtTimestamp?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_not?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_gt?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_lt?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_gte?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_lte?: InputMaybe<Scalars['BigInt']>;
    createdAtTimestamp_in?: InputMaybe<Array<Scalars['BigInt']>>;
    createdAtTimestamp_not_in?: InputMaybe<Array<Scalars['BigInt']>>;
    /** Filter for the block changed event. */
    _change_block?: InputMaybe<BlockChangedFilter>;
  };

  export type Transfer_orderBy =
    | 'id'
    | 'domain'
    | 'owner'
    | 'eventType'
    | 'createdAtTimestamp';

  export type _Block_ = {
    /** The hash of the block */
    hash?: Maybe<Scalars['Bytes']>;
    /** The block number */
    number: Scalars['Int'];
    /** Integer representation of the timestamp stored in blocks for the chain */
    timestamp?: Maybe<Scalars['Int']>;
  };

  /** The type for the top-level _meta field */
  export type _Meta_ = {
    /**
     * Information about a specific subgraph block. The hash of the block
     * will be null if the _meta field has a block constraint that asks for
     * a block number. It will be filled if the _meta field has no block constraint
     * and therefore asks for the latest  block
     *
     */
    block: _Block_;
    /** The deployment ID */
    deployment: Scalars['String'];
    /** If `true`, the subgraph encountered indexing errors at some past block */
    hasIndexingErrors: Scalars['Boolean'];
  };

  export type _SubgraphErrorPolicy_ =
    /** Data will be returned even if the subgraph has indexing errors */
    | 'allow'
    /** If the subgraph has indexing errors, data will be omitted. The default. */
    | 'deny';

  export type QuerySdk = {
    /** undefined **/
    domain: InContextSdkMethod<Query['domain'], QuerydomainArgs, MeshContext>;
    /** undefined **/
    domains: InContextSdkMethod<
      Query['domains'],
      QuerydomainsArgs,
      MeshContext
    >;
    /** undefined **/
    transfer: InContextSdkMethod<
      Query['transfer'],
      QuerytransferArgs,
      MeshContext
    >;
    /** undefined **/
    transfers: InContextSdkMethod<
      Query['transfers'],
      QuerytransfersArgs,
      MeshContext
    >;
    /** Access to subgraph metadata **/
    _meta: InContextSdkMethod<Query['_meta'], Query_metaArgs, MeshContext>;
  };

  export type MutationSdk = {};

  export type SubscriptionSdk = {
    /** undefined **/
    domain: InContextSdkMethod<
      Subscription['domain'],
      SubscriptiondomainArgs,
      MeshContext
    >;
    /** undefined **/
    domains: InContextSdkMethod<
      Subscription['domains'],
      SubscriptiondomainsArgs,
      MeshContext
    >;
    /** undefined **/
    transfer: InContextSdkMethod<
      Subscription['transfer'],
      SubscriptiontransferArgs,
      MeshContext
    >;
    /** undefined **/
    transfers: InContextSdkMethod<
      Subscription['transfers'],
      SubscriptiontransfersArgs,
      MeshContext
    >;
    /** Access to subgraph metadata **/
    _meta: InContextSdkMethod<
      Subscription['_meta'],
      Subscription_metaArgs,
      MeshContext
    >;
  };

  export type Context = {
    ['subgraph']: {
      Query: QuerySdk;
      Mutation: MutationSdk;
      Subscription: SubscriptionSdk;
    };
  };
}
