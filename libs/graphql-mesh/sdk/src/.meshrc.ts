import type { YamlConfig } from '@graphql-mesh/types';
import { resolve } from 'path';

import backendSchema from './schemas/backend.schema.json';
import subgraphSchema from './schemas/subgraph.schema.json';

const subgraphHandler: YamlConfig.GraphQLHandlerHTTPConfiguration = {
  endpoint: process.env.NX_APP_SUBGRAPH_URL,
  source: JSON.stringify(subgraphSchema),
};

const backendHandler: YamlConfig.GraphQLHandlerHTTPConfiguration = {
  endpoint: process.env.NX_APP_BACKEND_URL,
  source: JSON.stringify(backendSchema),
};

const config: YamlConfig.Config = {
  sources: [
    {
      name: 'subgraph',
      handler: { graphql: subgraphHandler },
    },
    {
      name: 'backend',
      handler: {
        graphql: backendHandler,
      },
    },
  ],
  additionalTypeDefs: resolve(__dirname, 'additionalTypeDefs.graphql'),
};

export default config;
