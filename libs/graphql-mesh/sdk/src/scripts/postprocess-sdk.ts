import { command } from 'execa';
import { readFile, writeFile } from 'fs/promises';
import { resolve } from 'path';

/**
 * remove .ts extension from meshrc import
 */
async function postprocessSdk() {
  await command(`rm -rf mesh && mv .mesh mesh`, {
    shell: true,
    cwd: resolve(__dirname, '..'),
  });

  const sdkIndexPath = resolve(__dirname, '..', 'mesh', 'index.ts');

  const sdkIndex = await readFile(sdkIndexPath, { encoding: 'utf-8' });

  const meshrcImportRegex = /import\("(.*)meshrc.ts"\)/gi;

  const indexWithRewrittenMeshrcImport = sdkIndex.replace(
    meshrcImportRegex,
    `import("$1meshrc")`
  );

  await writeFile(sdkIndexPath, indexWithRewrittenMeshrcImport);
}

postprocessSdk();
