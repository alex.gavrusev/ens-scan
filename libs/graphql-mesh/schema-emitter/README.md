# graphql-mesh-schema-emitter

This library was generated with [Nx](https://nx.dev).

## Running lint

Run `nx lint graphql-mesh-schema-emitter` to execute the lint via [ESLint](https://eslint.org/).
