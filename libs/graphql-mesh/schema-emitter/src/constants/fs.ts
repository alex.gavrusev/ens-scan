import { resolve } from 'path';
import { workspaceRoot } from '@nrwl/devkit';

export const schemaEmitPath = resolve(
  workspaceRoot,
  'libs',
  'graphql-mesh',
  'sdk',
  'src',
  'schemas'
);
