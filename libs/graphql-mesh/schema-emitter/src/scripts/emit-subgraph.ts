import fetch from 'cross-fetch';
import { getIntrospectionQuery, buildClientSchema } from 'graphql';
import { mkdir, writeFile } from 'fs/promises';
import { resolve } from 'path';

import { schemaEmitPath } from '../constants';
import { getDocumentNodeFileContents } from '../utils';

async function emitSubgraphSchema() {
  const introspectionQuery = getIntrospectionQuery();

  const response = await fetch(process.env.NX_APP_SUBGRAPH_URL, {
    method: 'POST',
    body: JSON.stringify({ query: introspectionQuery }),
  });

  const data = await response.json();
  const schema = buildClientSchema(data.data);

  await mkdir(schemaEmitPath, { recursive: true });
  await writeFile(
    resolve(schemaEmitPath, 'subgraph.schema.json'),
    getDocumentNodeFileContents(schema)
  );
}

emitSubgraphSchema();
