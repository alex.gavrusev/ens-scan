import { NestFactory } from '@nestjs/core';
import {
  GraphQLSchemaBuilderModule,
  GraphQLSchemaFactory,
} from '@nestjs/graphql';
import { mkdir, writeFile } from 'fs/promises';
import { resolve } from 'path';

import { EnsResolver } from '@ens-scan/backend/api/ens';

import { schemaEmitPath } from '../constants';
import { getDocumentNodeFileContents } from '../utils';

async function emitSchema() {
  const app = await NestFactory.create(GraphQLSchemaBuilderModule);
  await app.init();

  const gqlSchemaFactory = app.get(GraphQLSchemaFactory);
  const schema = await gqlSchemaFactory.create([EnsResolver]);

  await mkdir(schemaEmitPath, { recursive: true });
  await writeFile(
    resolve(schemaEmitPath, 'backend.schema.json'),
    getDocumentNodeFileContents(schema)
  );
}

emitSchema();
