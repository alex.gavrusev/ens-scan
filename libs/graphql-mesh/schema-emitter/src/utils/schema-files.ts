import { GraphQLSchema } from 'graphql';
import { getDocumentNodeFromSchema } from '@graphql-tools/utils';

export const getDocumentNodeFileContents = (schema: GraphQLSchema) =>
  JSON.stringify(getDocumentNodeFromSchema(schema), null, 2);
