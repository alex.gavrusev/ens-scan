import { IsInt, IsEmail } from 'class-validator';

import { classTransformerValidatorThrowing } from '../class-transformer-validator-throwing';

describe('classTransformerValidatorThrowing', () => {
  class TestClass {
    @IsInt()
    id!: number;

    @IsEmail()
    email!: string;
  }

  const validValue = {
    id: '1',
    email: 'test@email.com',
  };

  const invalidValue = {
    id: 'junk',
    email: 'junk',
  };

  const validate = classTransformerValidatorThrowing(TestClass);

  it('does not throw for valid values', () => {
    expect(() => validate(validValue)).not.toThrow();
  });

  it('throws for invalid values', () => {
    expect(() => validate(invalidValue)).toThrow();
  });

  it('converts values', () => {
    const validated = validate(validValue);

    expect(validated.id).toBe(1);
  });
});
