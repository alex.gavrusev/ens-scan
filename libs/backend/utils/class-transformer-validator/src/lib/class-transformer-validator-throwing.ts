import * as E from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';

import { ClassTransformOptions } from 'class-transformer';
import { ValidatorOptions } from 'class-validator';

import { classTransformerValidator } from './class-transformer-validator';

export const classTransformerValidatorThrowing =
  <C extends new (...args: any[]) => object>(
    cls: C,
    transformOptions: ClassTransformOptions = {
      enableImplicitConversion: true,
    },
    validatorOptions: ValidatorOptions = { skipMissingProperties: false }
  ) =>
  (value: object) => {
    const either = pipe(
      value,
      classTransformerValidator(cls, transformOptions, validatorOptions)
    );

    if (E.isLeft(either)) {
      throw new Error(either.left.toString());
    }

    return either.right;
  };
