/* eslint-disable @typescript-eslint/ban-types */
import * as E from 'fp-ts/Either';

import { plainToClass, ClassTransformOptions } from 'class-transformer';
import {
  validateSync,
  ValidationError,
  ValidatorOptions,
} from 'class-validator';

export const classTransformerValidator =
  <C extends new (...args: any[]) => object>(
    cls: C,
    transformOptions: ClassTransformOptions = {
      enableImplicitConversion: true,
    },
    validatorOptions: ValidatorOptions = { skipMissingProperties: false }
  ) =>
  (value: object): E.Either<ValidationError[], InstanceType<C>> => {
    const validatedValue = plainToClass(cls, value, transformOptions);

    const errors = validateSync(validatedValue, validatorOptions);

    if (errors.length > 0) {
      return E.left(errors);
    }

    return E.right(validatedValue as InstanceType<C>);
  };
