import { getJestConfig } from '@ens-scan/config/test/node';

export default getJestConfig('backend-utils-class-transformer-validator');
