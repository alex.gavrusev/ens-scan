import { IsUrl } from 'class-validator';

export class EthersConfig {
  @IsUrl()
  infuraUrl!: string;
}
