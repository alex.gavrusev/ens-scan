import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ethersConfigFactory } from './ethers-config.factory';

@Module({
  imports: [ConfigModule.forFeature(ethersConfigFactory)],
  exports: [ConfigModule],
})
export class EthersConfigModule {}
