import { registerAs } from '@nestjs/config';

import { classTransformerValidatorThrowing } from '@ens-scan/backend/utils/class-transformer-validator';

import { EthersConfig } from './ethers.config';

export const ethersConfigFactory = registerAs('ethers', () =>
  classTransformerValidatorThrowing(EthersConfig)({
    infuraUrl: process.env.NX_APP_INFURA_URL,
  })
);
