import { Injectable } from '@nestjs/common';
import { providers } from 'ethers';

@Injectable()
export class EthersProviderService extends providers.JsonRpcProvider {}
