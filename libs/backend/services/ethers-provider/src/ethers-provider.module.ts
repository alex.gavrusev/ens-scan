import { Module } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';

import { ethersConfigFactory, EthersConfigModule } from './ethers-config';
import { EthersProviderService } from './ethers-provider.service';

@Module({
  imports: [EthersConfigModule],
  providers: [
    {
      provide: EthersProviderService,
      useFactory: (config: ConfigType<typeof ethersConfigFactory>) =>
        new EthersProviderService(config.infuraUrl),
      inject: [ethersConfigFactory.KEY],
    },
  ],
  exports: [EthersProviderService],
})
export class EthersProviderModule {}
