# backend-services-ethers-provider

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build backend-services-ethers-provider` to build the library.

## Running unit tests

Run `nx test backend-services-ethers-provider` to execute the unit tests via [Jest](https://jestjs.io).
