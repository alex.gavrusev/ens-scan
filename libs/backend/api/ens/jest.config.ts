import { getJestConfig } from '@ens-scan/config/test/node';

export default getJestConfig('backend-api-ens');
