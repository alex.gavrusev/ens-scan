# backend-api-ens

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build backend-api-ens` to build the library.

## Running unit tests

Run `nx test backend-api-ens` to execute the unit tests via [Jest](https://jestjs.io).
