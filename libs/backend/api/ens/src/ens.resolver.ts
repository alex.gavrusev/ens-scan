import { EthersProviderService } from '@ens-scan/backend/services/ethers-provider';
import { Args, Query, Resolver } from '@nestjs/graphql';

@Resolver()
export class EnsResolver {
  constructor(private readonly ethersProviderService: EthersProviderService) {}

  @Query(() => String, { nullable: true })
  ethereumName(@Args('address', { type: () => String }) address: string) {
    return this.ethersProviderService.lookupAddress(address);
  }
}
