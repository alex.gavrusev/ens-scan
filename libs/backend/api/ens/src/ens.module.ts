import { Module } from '@nestjs/common';

import { EthersProviderModule } from '@ens-scan/backend/services/ethers-provider';

import { EnsResolver } from './ens.resolver';

@Module({
  imports: [EthersProviderModule],
  providers: [EnsResolver],
  exports: [],
})
export class EnsModule {}
