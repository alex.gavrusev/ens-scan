# config-test-web

This library was generated with [Nx](https://nx.dev).

## Running lint

Run `nx lint config-test-web` to execute the lint via [ESLint](https://eslint.org/).
