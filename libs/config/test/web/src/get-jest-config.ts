import type { Config } from 'jest';
import { resolve } from 'path';

import { swcrc } from '@ens-scan/config/build/web/swcrc';
import {
  getCoverageDirectory,
  getWorkspacePresetPath,
} from '@ens-scan/config/test/utils';

const { jsc } = swcrc;

export const getJestConfig = (displayName: string): Config => {
  /**
   * HACK: require.main won't work, jest config is not the script entrypoint
   * since jest.config.ts is in the root of a project, this will be the complete project path
   *
   * parent 1 — index
   * parent 2 - jest config
   */
  const projectDirectory = module.parent?.parent?.path;

  if (!projectDirectory) {
    throw new Error('Unable to resolve project directory');
  }

  return {
    displayName,
    preset: getWorkspacePresetPath(projectDirectory),
    transform: {
      '^.+\\.[tj]sx?$': ['@swc/jest', { jsc }],
    },
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
    coverageDirectory: getCoverageDirectory(projectDirectory),
    setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
    setupFiles: [resolve(__dirname, 'storybook-setup.ts')],
    testEnvironment: 'jsdom',
  };
};
