import { setGlobalConfig } from '@storybook/testing-react';

import * as globalStorybookConfig from '@ens-scan/config/storybook/preview';

setGlobalConfig(globalStorybookConfig);
