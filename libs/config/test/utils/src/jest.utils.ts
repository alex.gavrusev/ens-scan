import { join, relative } from 'path';
import { workspaceRoot } from '@nrwl/devkit';

export const getWorkspacePresetPath = (projectDirectory: string) => {
  console.log(
    'P DIR',
    projectDirectory,
    join(workspaceRoot, 'jest.preset.js'),
    relative(projectDirectory, join(workspaceRoot, 'jest.preset.js'))
  );

  return relative(projectDirectory, join(workspaceRoot, 'jest.preset.js'));
};

export const getCoverageDirectory = (projectDirectory: string) => {
  const projectPathFromRoot = relative(workspaceRoot, projectDirectory);

  return relative(
    projectDirectory,
    join(workspaceRoot, 'coverage', projectPathFromRoot)
  );
};
