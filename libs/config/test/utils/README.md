# config-test-utils

This library was generated with [Nx](https://nx.dev).

## Running lint

Run `nx lint config-test-utils` to execute the lint via [ESLint](https://eslint.org/).
