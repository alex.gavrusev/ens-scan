import type { DecoratorFn } from '@storybook/react';
import { ChakraProvider } from '@chakra-ui/react';
import { BrowserRouter } from 'react-router-dom';

import { theme } from '@ens-scan/frontend/ui/theme';

import { mswDecorator } from './msw';
import { urqlDecorator } from './urql';

export const decorators: DecoratorFn[] = [
  mswDecorator,
  urqlDecorator,
  (Story) => (
    <ChakraProvider theme={theme}>
      <BrowserRouter>
        <Story />
      </BrowserRouter>
    </ChakraProvider>
  ),
];
