import type { DecoratorFn } from '@storybook/react';
import { useState } from 'react';
import { Provider as UrqlProvider } from 'urql';

import { createNoCacheUrqlClient } from '@ens-scan/frontend/graphql/clients';

const UrqlDecorator: DecoratorFn = (Story) => {
  // create a new client for each story
  const [client] = useState(() => createNoCacheUrqlClient());

  return (
    <UrqlProvider value={client}>
      <Story />
    </UrqlProvider>
  );
};

export { UrqlDecorator as urqlDecorator };
