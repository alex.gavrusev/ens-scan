import type { StorybookConfig } from '@storybook/core-common';
import { workspaceRoot, readJsonFile } from '@nrwl/devkit';
import { resolve } from 'path';

import { swcrc } from '@ens-scan/config/build/web/swcrc';

const {
  // rm externalHelpers, panics otherwise
  jsc: { externalHelpers: _, ...jsc },
  sourceMaps,
} = swcrc;

const packageJson = readJsonFile(resolve(workspaceRoot, 'package.json'));
let mswDir: string | null = null;
try {
  mswDir = resolve(workspaceRoot, packageJson.msw.workerDirectory);
} catch (e) {
  console.error('Unable to get msw dir');
  throw e;
}

console.log(resolve(workspaceRoot, ''));

const config: StorybookConfig = {
  core: { builder: 'webpack5' },
  // serve MSW as an asset, as per the docs
  staticDirs: [mswDir],

  stories: [
    'apps/frontend/src/app/**/*.stories.mdx',
    'apps/frontend/src/app/**/*.stories.@(js|jsx|ts|tsx)',
    'libs/frontend/**/*.stories.mdx',
    'libs/frontend/**/*.stories.@(js|jsx|ts|tsx)',
  ].map((path) => resolve(workspaceRoot, path)),

  addons: [
    '@storybook/addon-essentials',
    '@nrwl/react/plugins/storybook',
    {
      name: 'storybook-addon-swc',
      options: {
        swcLoaderOptions: {
          jsc,
          sourceMaps,
          // webpack fails otherwise
          module: { type: 'commonjs' },
        },
      },
    },
  ],
};

module.exports = config;
