import type { DecoratorFn } from '@storybook/react';
import { useEffect, useState } from 'react';
import { RequestHandler } from 'msw';

import { useWorkerOrServer } from './use-worker-or-server';

type DecoratorParameters = {
  msw?: { handlers: RequestHandler[] | Record<string, RequestHandler> };
};

const MswDecorator: DecoratorFn = (Story, context) => {
  const { msw } = context.parameters as DecoratorParameters;

  const workerOrServer = useWorkerOrServer();

  // the initial value would be true if no handlers are specified
  const [didInit, setDidInit] = useState(!msw?.handlers);

  useEffect(() => {
    if (didInit) {
      return;
    }

    // NOTE(`??`): just to make the lint shut up
    const handlers = Object.values(msw?.handlers ?? {})
      .filter(Boolean)
      .reduce(
        (handlers, handlersList) => handlers.concat(handlersList),
        [] as RequestHandler[]
      );

    if (handlers.length > 0) {
      'start' in workerOrServer
        ? workerOrServer.start().finally(() => setDidInit(true))
        : workerOrServer.listen();

      workerOrServer.use(...handlers);
    } else {
      setDidInit(true);
    }
  }, [didInit, msw?.handlers, workerOrServer]);

  // wait for msw init, to not let through any requests that should be intercepted
  if (!didInit) {
    return <></>;
  }

  return <Story />;
};

export { MswDecorator as mswDecorator };
