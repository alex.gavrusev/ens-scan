import { setupWorker, SetupWorkerApi } from 'msw';
import type { SetupServerApi } from 'msw/node';
import { useEffect, useMemo } from 'react';

export const useWorkerOrServer = () => {
  const workerOrServer: SetupWorkerApi | SetupServerApi = useMemo(() => {
    const isNode = !!process?.versions?.node;

    /**
     * Workaround as to not load the node version in the browser,
     * and for webpack to not error during build
     *
     * @see https://github.com/webpack/webpack/issues/8826#issuecomment-660594260
     */
    const nodeRequire = isNode
      ? typeof __webpack_require__ === 'function'
        ? __non_webpack_require__
        : require
      : undefined;

    return isNode ? nodeRequire('msw/node').setupServer() : setupWorker();
  }, []);

  // Teardown on story unmount
  useEffect(
    () => () =>
      'stop' in workerOrServer ? workerOrServer.stop() : workerOrServer.close(),
    [workerOrServer]
  );

  return workerOrServer;
};
