import { Configuration, IgnorePlugin } from 'webpack';
import merge from 'webpack-merge';
import { U } from 'ts-toolbelt';
import { produce, setAutoFreeze } from 'immer';

setAutoFreeze(false);

export function assertStringFilename(
  filename: unknown
): asserts filename is string {
  if (typeof filename !== 'string') {
    throw new Error(
      `"output.filename" must be a string, got ${typeof filename}`
    );
  }
}

export const getIsEs5Build = (
  outputFilename: U.NonNullable<Configuration['output']>['filename']
): boolean => {
  assertStringFilename(outputFilename);

  return outputFilename.includes('es5');
};

export const setTargets = (config: Configuration) => {
  const isEs5Build = getIsEs5Build(config.output?.filename);

  return produce(config, (draft) => {
    draft.target = isEs5Build ? ['es5', 'web'] : ['web'];
  });
};

export const setResolveMainFields = (config: Configuration) => {
  const isEs5Build = getIsEs5Build(config.output?.filename);

  return produce(config, (draft) => {
    draft.resolve ??= {};
    draft.resolve.mainFields = isEs5Build
      ? ['main', 'browser']
      : ['browser', 'module', 'main'];
  });
};

export const setupIgnorePlugin = (config: Configuration) =>
  merge(config, {
    plugins: [
      /**
       * The ES5 build imports builable libs as CJS modules,
       * so tree-shaking fails.
       *
       * Since gql codegen also includes MSW utils
       * `msw` has to be manually ignored, both for bundle size reasons,
       * and because it probably won't work in the browser
       */
      new IgnorePlugin({
        checkResource(resource) {
          return [
            // graphql-mesh
            'path',
            'fs',
            'stream',
            'crypto',
            'http',
            'https',
            'zlib',
            'tty',
            'console',
            'diagnostics_channel',
            'bufferutil',
            'child_process',
            'ws',
            'async_hooks',
            'tls',
            'net',
            'perf_hooks',
            'worker_threads',
            'util',
            'utf-8-validate',
            'stream/web',
            'util/types',
          ].includes(resource);
        },
      }),
    ],
  });

export const setupResolveFallback = (config: Configuration) =>
  merge(config, {
    resolve: {
      fallback: {
        // msw
        msw: false,
      },
    },
  });
