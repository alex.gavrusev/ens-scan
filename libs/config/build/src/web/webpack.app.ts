import { NormalizedWebpackExecutorOptions } from '@nrwl/webpack';
import getWebpackConfig from '@nrwl/react/plugins/webpack';
import { Configuration } from 'webpack';
import { pipe } from 'fp-ts/function';
import { JsMinifyOptions } from '@swc/core';

import {
  replaceSwcLoader,
  setupBundleAnalyzer,
  setupTerser,
} from '../shared/webpack-utils';

import {
  setupIgnorePlugin,
  setTargets,
  setResolveMainFields,
  getIsEs5Build,
  setupResolveFallback,
} from './webpack-utils';
import { swcrc } from './swcrc';

const {
  // Minification will be done by swc from terser
  jsc: { minify, ...jsc },
  sourceMaps,
  env,
  module: swcMod,
} = swcrc;

const getAppWebpackConfig = (
  config: Configuration,
  context:
    | /** regular build */ { options: NormalizedWebpackExecutorOptions }
    | /** dev server target */ {
        buildOptions: NormalizedWebpackExecutorOptions;
      }
) => {
  // HACK: there's no named `getWebpackConfig` export, only a default one
  const webpackConfig: Configuration = (getWebpackConfig as any)(config);

  const { compiler, optimization } =
    'buildOptions' in context ? context.buildOptions : context.options;

  const isEs5Build = getIsEs5Build(webpackConfig.output?.filename);

  return pipe(
    webpackConfig,
    setTargets,
    setResolveMainFields,
    replaceSwcLoader(compiler, (rule) => ({
      ...rule,
      options: {
        jsc,
        env: {
          ...env,
          targets: isEs5Build ? 'IE 11' : env.targets,
        },
        sourceMaps,
        module: isEs5Build ? { type: 'commonjs' } : swcMod,
      },
      // transpile node_modules to get proper es5 throughout
      exclude: isEs5Build ? undefined : rule.exclude,
    })),
    setupTerser(optimization, minify as JsMinifyOptions),
    setupBundleAnalyzer(optimization, {
      // because of differential loading, the report for the es5 would override the esm report
      reportFilename: isEs5Build ? 'report.es5.html' : 'report.html',
    }),
    setupIgnorePlugin,
    setupResolveFallback
  );
};

export default getAppWebpackConfig;
