import { readFileSync } from 'fs';
import { resolve } from 'path';

export const swcrc: typeof import('./.swcrc.json') = JSON.parse(
  readFileSync(resolve(__dirname, '.swcrc.json')).toString()
);
