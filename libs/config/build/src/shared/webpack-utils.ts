import { NormalizedWebpackExecutorOptions } from '@nrwl/webpack';
import { ModuleOptions, Configuration, RuleSetRule } from 'webpack';
import TerserPlugin, { MinimizerOptions } from 'terser-webpack-plugin';
import merge from 'webpack-merge';
import { JsMinifyOptions } from '@swc/core';
import { produce, setAutoFreeze } from 'immer';
import { U } from 'ts-toolbelt';
import { gray } from 'chalk';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

setAutoFreeze(false);

function assertSwcCompiler(
  compiler: NormalizedWebpackExecutorOptions['compiler']
): asserts compiler is 'swc' {
  if (compiler !== 'swc') {
    throw new Error(`"compiler" must be set to "swc"`);
  }
}

function assertRulesPresent(
  rules: ModuleOptions['rules']
): asserts rules is U.NonNullable<typeof rules> {
  if (!rules) {
    throw new Error(`"module.rules" is not defined`);
  }
}

export const replaceSwcLoader =
  (
    compiler: NormalizedWebpackExecutorOptions['compiler'],
    replaceFn: (swcRule: RuleSetRule) => RuleSetRule
  ) =>
  (config: Configuration) =>
    produce(config, (draft: Configuration) => {
      assertSwcCompiler(compiler);
      assertRulesPresent(draft.module?.rules);

      draft.module.rules = draft.module.rules.map((rule) => {
        if (typeof rule !== 'object' || !rule.loader?.match('swc-loader')) {
          return rule;
        }

        console.log(gray('replacing default swc-loader'));

        return replaceFn(rule);
      });
    });

const hasEnabledOptimization = (
  optimization: NormalizedWebpackExecutorOptions['optimization']
) => {
  if (typeof optimization === 'object') {
    return optimization.scripts || optimization.styles;
  }

  return !!optimization;
};

export const setupTerser =
  (
    optimization: NormalizedWebpackExecutorOptions['optimization'],
    minify: JsMinifyOptions
  ) =>
  (config: Configuration) => {
    if (!hasEnabledOptimization(optimization)) {
      return config;
    }

    return merge(config, {
      optimization: {
        minimizer: [
          new TerserPlugin<JsMinifyOptions>({
            minify: TerserPlugin.swcMinify,
            terserOptions: minify as MinimizerOptions<JsMinifyOptions>,
          }),
        ],
      },
    });
  };

export const setupBundleAnalyzer =
  (
    optimization: NormalizedWebpackExecutorOptions['optimization'],
    analyzerOptions?: Partial<BundleAnalyzerPlugin.Options>
  ) =>
  (config: Configuration) => {
    if (!hasEnabledOptimization(optimization)) {
      return config;
    }

    return merge(config, {
      plugins: [
        new BundleAnalyzerPlugin({
          analyzerMode: 'static',
          openAnalyzer: false,
          defaultSizes: 'gzip',
          ...analyzerOptions,
        }),
      ],
    });
  };
