import { Configuration, DefinePlugin } from 'webpack';
import { merge } from 'webpack-merge';

import { getClientEnvironment } from '@nrwl/webpack/src/utils/config';

export const addDefinePlugin = (config: Configuration) =>
  merge(config, {
    plugins: [
      new DefinePlugin(
        getClientEnvironment(config.mode ?? 'production').stringified
      ),
    ],
  });
