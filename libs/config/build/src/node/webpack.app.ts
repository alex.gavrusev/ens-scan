import { NormalizedWebpackExecutorOptions } from '@nrwl/webpack';
import { Configuration } from 'webpack';
import { pipe } from 'fp-ts/function';
import { JsMinifyOptions } from '@swc/core';

import {
  replaceSwcLoader,
  setupBundleAnalyzer,
  setupTerser,
} from '../shared/webpack-utils';

import { swcrc } from './swcrc';
import { addDefinePlugin } from './webpack-utils';

const {
  // Minification will be done by swc from terser
  jsc: { minify, ...jsc },
  sourceMaps,
  module: swcMod,
} = swcrc;

const getAppWebpackConfig = (
  config: Configuration,
  context: { options: NormalizedWebpackExecutorOptions }
) => {
  const {
    options: { compiler, optimization },
  } = context;

  return pipe(
    config,
    replaceSwcLoader(compiler, (rule) => ({
      ...rule,
      options: {
        jsc,
        sourceMaps,
        module: swcMod,
      },
    })),
    addDefinePlugin,
    setupTerser(optimization, minify as JsMinifyOptions),
    setupBundleAnalyzer(optimization)
  );
};

export default getAppWebpackConfig;
