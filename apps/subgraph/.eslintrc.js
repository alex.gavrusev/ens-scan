// inspired by https://github.com/AssemblyScript/assemblyscript/blob/main/.eslintrc.cjs
module.exports = {
  extends: ['../../.eslintrc.json'],
  ignorePatterns: ['!**/*'],
  overrides: [
    {
      /** AssemblyScript rules */
      files: ['*.ts'],
      rules: {
        // This tends to be annoying as it encourages developers to make everything
        // that is never reassigned a 'const', sometimes semantically incorrect so
        'prefer-const': 'off',

        // There is actually codegen difference here
        '@typescript-eslint/no-array-constructor': 'off',

        // Sometimes it can't be avoided to add a @ts-ignore
        '@typescript-eslint/ban-ts-comment': 'off',

        // Utilized to achieve portability in some cases
        '@typescript-eslint/no-non-null-assertion': 'off',

        // There is an actual codegen difference here - TODO: revisit
        'no-cond-assign': 'off',

        // Not all types can be omitted in AS yet - TODO: revisit
        '@typescript-eslint/no-inferrable-types': 'off',

        // The compiler has its own `Function` class for example
        'no-shadow-restricted-names': 'off',
        '@typescript-eslint/ban-types': 'off',
      },
    },
  ],
};
