import {
  NewOwner as NewOwnerEvent,
  Transfer as TransferEvent,
} from '../../generated/Registry/Registry';

import { domainService } from '../domain';
import { transferService } from '../transfer';

export function handleNewOwner(event: NewOwnerEvent): void {
  const domain = domainService.handleNewOwner(
    event.params.node,
    event.params.label,
    event.params.owner,
    event.block.timestamp
  );

  if (domain == null) {
    return;
  }

  transferService.handleNewOwner(
    domain,
    event.logIndex,
    event.transaction.hash,
    event.block.timestamp
  );
}

export function handleTransfer(event: TransferEvent): void {
  transferService.handleTransfer(
    event.params.node,
    event.params.owner,
    event.logIndex,
    event.transaction.hash,
    event.block.timestamp
  );
}
