import { Address, BigInt, Bytes, crypto, ens } from '@graphprotocol/graph-ts';

import { Domain } from '../../generated/schema';

import { concat } from '../utils/bytes';

import { domainLogger, DomainLogger } from './domain.logger';
import { domainRepository, DomainRepository } from './domain.repository';

export class DomainService {
  constructor(
    private readonly repository: DomainRepository,
    private readonly logger: DomainLogger
  ) {}

  private isRootNode(node: Bytes): boolean {
    return (
      node.toHexString() ==
      '0x0000000000000000000000000000000000000000000000000000000000000000'
    );
  }

  private getSubnodeId(nodeId: Bytes, label: Bytes): string {
    // bytes32 subnode = keccak256(abi.encodePacked(node, label));
    return crypto.keccak256(concat(nodeId, label)).toHexString();
  }

  private getName(label: Bytes, parent: Domain): string | null {
    let name = ens.nameByHash(label.toHexString());
    if (parent.name != null) {
      // fallback for things like *.addr.reverse
      if (name == null) {
        name = '[' + label.toHexString().slice(2) + ']';
      }

      // HACK(non-null): asc fails to refine types to non-nullable
      name = name + '.' + parent.name!;
    }

    return name;
  }

  private getOrCreateParent(parentNode: Bytes): Domain | null {
    const nodeId = parentNode.toHexString();

    const existing = this.repository.findOne(nodeId);

    if (existing != null) {
      return existing;
    }

    if (this.isRootNode(parentNode)) {
      return this.repository.create(
        nodeId,
        null,
        // will be transferred to the Base registrar after a subsequent `Transfer` event
        Address.zero().toHexString(),
        null,
        // no real usable timestamp
        BigInt.zero()
      );
    }

    return null;
  }

  findOne(id: string): Domain | null {
    return this.repository.findOne(id);
  }

  handleNewOwner(
    node: Bytes,
    label: Bytes,
    owner: Address,
    blockTimestamp: BigInt
  ): Domain | null {
    const subnodeId = this.getSubnodeId(node, label);

    const domain = this.repository.findOne(subnodeId);

    const parent = this.getOrCreateParent(node);
    if (parent == null) {
      this.logger.parentNotFound(subnodeId);
      return null;
    }

    let name = this.getName(label, parent);

    if (domain == null) {
      return this.repository.create(
        subnodeId,
        name,
        owner.toHexString(),
        parent.id,
        blockTimestamp
      );
    }

    domain.parent = parent.id;
    domain.name = name;
    domain.owner = owner.toHexString();
    domain.save();
    return domain;
  }
}

export const domainService = new DomainService(domainRepository, domainLogger);
