import { log } from '@graphprotocol/graph-ts';
import { Domain } from '../../generated/schema';

import { EntityLogger } from '../entity-logger';

export class DomainLogger extends EntityLogger<Domain> {
  constructor() {
    super('Domain');
  }

  parentNotFound(id: string): void {
    log.error('parent for node id {} was not found', [id]);
  }
}

export const domainLogger = new DomainLogger();
