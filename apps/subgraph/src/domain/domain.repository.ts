import { BigInt } from '@graphprotocol/graph-ts';

import { Domain } from '../../generated/schema';

import { DomainLogger, domainLogger } from './domain.logger';

export class DomainRepository {
  constructor(private readonly logger: DomainLogger) {}

  findOne(id: string): Domain | null {
    const domain = Domain.load(id);

    this.logger.ifNotFound(id, domain);

    return domain;
  }

  private save(domain: Domain): Domain {
    this.logger.saving(domain.id);
    domain.save();

    return domain;
  }

  create(
    id: string,
    name: string | null,
    owner: string,
    parent: string | null,
    createdAtTimestamp: BigInt
  ): Domain {
    this.logger.creating(id);

    const domain = new Domain(id);
    domain.name = name;
    domain.owner = owner;
    domain.parent = parent;
    domain.createdAtTimestamp = createdAtTimestamp;

    this.save(domain);

    return domain;
  }
}

export const domainRepository = new DomainRepository(domainLogger);
