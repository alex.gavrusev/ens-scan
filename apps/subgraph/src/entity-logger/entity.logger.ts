import { log } from '@graphprotocol/graph-ts';

export class EntityLogger<T> {
  constructor(private entityName: string) {}

  creating(id: string): void {
    log.info('Creating {} with id {}', [this.entityName, id]);
  }

  saving(id: string): void {
    log.info('Saving {} with id {}', [this.entityName, id]);
  }

  ifNotFound(id: string, entity: T | null): void {
    if (entity == null) {
      log.error('{} with id {} was not found', [this.entityName, id]);
    }
  }
}
