import { log } from '@graphprotocol/graph-ts';

import { Transfer } from '../../generated/schema';

import { EntityLogger } from '../entity-logger';

export class TransferLogger extends EntityLogger<Transfer> {
  constructor() {
    super('Transfer');
  }

  domainForTransferNotFound(id: string): void {
    log.critical('Domain for Transfer {} was not found', [id]);
  }
}

export const transferLogger = new TransferLogger();
