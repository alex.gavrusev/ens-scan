import { Address, BigInt, Bytes } from '@graphprotocol/graph-ts';

import { Domain } from '../../generated/schema';

import { domainService, DomainService } from '../domain';

import { transferLogger, TransferLogger } from './transfer.logger';
import { transferRepository, TransferRepository } from './transfer.repository';

export class TransferService {
  constructor(
    private readonly repository: TransferRepository,
    private readonly domainService: DomainService,
    private readonly logger: TransferLogger
  ) {}

  private createId(eventLogIndex: BigInt, transactionHash: Bytes): string {
    return (
      transactionHash.toHexString().slice(2) + '-' + eventLogIndex.toString()
    );
  }

  private handleEvent(
    node: Bytes,
    owner: Address,
    eventLogIndex: BigInt,
    transactionHash: Bytes,
    blockTimestamp: BigInt,
    eventType: string
  ): void {
    const id = this.createId(eventLogIndex, transactionHash);

    const domain = this.domainService.findOne(node.toHexString());

    if (domain == null) {
      this.logger.domainForTransferNotFound(id);
      return;
    }

    this.repository.create(
      id,
      domain.id,
      owner.toHexString(),
      eventType,
      blockTimestamp
    );
  }

  handleNewOwner(
    domain: Domain,
    eventLogIndex: BigInt,
    transactionHash: Bytes,
    blockTimestamp: BigInt
  ): void {
    this.handleEvent(
      Bytes.fromHexString(domain.id),
      Address.fromBytes(Bytes.fromHexString(domain.owner)),
      eventLogIndex,
      transactionHash,
      blockTimestamp,
      'NewOwner'
    );
  }

  handleTransfer(
    node: Bytes,
    owner: Address,
    eventLogIndex: BigInt,
    transactionHash: Bytes,
    blockTimestamp: BigInt
  ): void {
    this.handleEvent(
      node,
      owner,
      eventLogIndex,
      transactionHash,
      blockTimestamp,
      'Transfer'
    );
  }
}

export const transferService = new TransferService(
  transferRepository,
  domainService,
  transferLogger
);
