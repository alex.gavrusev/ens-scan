import { BigInt } from '@graphprotocol/graph-ts';

import { Transfer } from '../../generated/schema';

import { EntityLogger } from '../entity-logger';

export class TransferRepository {
  private logger: EntityLogger<Transfer>;

  constructor() {
    this.logger = new EntityLogger('Transfer');
  }

  findOne(id: string): Transfer | null {
    const transfer = Transfer.load(id);

    this.logger.ifNotFound(id, transfer);

    return transfer;
  }

  private save(transfer: Transfer): Transfer {
    this.logger.saving(transfer.id);
    transfer.save();

    return transfer;
  }

  create(
    id: string,
    domain: string,
    owner: string,
    // actually `'NewOwner' | 'Transfer'`, but no unions/enums in AssemblyScript
    eventType: string,
    createdAtTimestamp: BigInt
  ): Transfer {
    this.logger.creating(id);

    const transfer = new Transfer(id);
    transfer.domain = domain;
    transfer.owner = owner;
    transfer.eventType = eventType;
    transfer.createdAtTimestamp = createdAtTimestamp;

    this.save(transfer);

    return transfer;
  }
}

export const transferRepository = new TransferRepository();
