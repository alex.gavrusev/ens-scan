import { ComponentType, FC, lazy, useMemo } from 'react';
import { Navigate, RouteObject, useRoutes } from 'react-router-dom';

import { ScreenSuspense } from '@ens-scan/frontend/ui/components/loading';

const reexportDefault =
  <K extends string, V extends ComponentType<any>>(path: K) =>
  (exported: Record<K, V>): { default: V } => ({ default: exported[path] });

const RecentTransfersScreen = lazy(() =>
  import('@ens-scan/frontend/ui/screens/recent-transfers').then(
    reexportDefault('RecentTransfersScreen')
  )
);
const DomainScreen = lazy(() =>
  import('@ens-scan/frontend/ui/screens/domain').then(
    reexportDefault('DomainScreen')
  )
);

export const AppRoutes: FC = () => {
  const routeConfig = useMemo<RouteObject[]>(
    () => [
      {
        path: '/',
        element: (
          <ScreenSuspense>
            <RecentTransfersScreen />
          </ScreenSuspense>
        ),
      },
      {
        path: '/domain/:id',
        element: (
          <ScreenSuspense>
            <DomainScreen />
          </ScreenSuspense>
        ),
      },
      {
        path: '*',
        element: <Navigate to="/" replace />,
      },
    ],
    []
  );

  return useRoutes(routeConfig);
};
