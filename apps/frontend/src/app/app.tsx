import { FC, StrictMode } from 'react';
import { Provider as UrqlProvider } from 'urql';
import { ChakraProvider } from '@chakra-ui/react';
import { BrowserRouter } from 'react-router-dom';

import { urqlClient } from '@ens-scan/frontend/graphql/clients';
import { theme } from '@ens-scan/frontend/ui/theme';
import { ScreenSuspense } from '@ens-scan/frontend/ui/components/loading';

import { AppRoutes } from './app-routes';

export const App: FC = () => {
  return (
    <StrictMode>
      <UrqlProvider value={urqlClient}>
        <ChakraProvider theme={theme}>
          <BrowserRouter>
            <ScreenSuspense>
              <AppRoutes />
            </ScreenSuspense>
          </BrowserRouter>
        </ChakraProvider>
      </UrqlProvider>
    </StrictMode>
  );
};
