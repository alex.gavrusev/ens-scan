import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';

import { getBuiltMesh } from '@ens-scan/graphql-mesh/sdk';

@Module({
  imports: [
    GraphQLModule.forRootAsync<ApolloDriverConfig>({
      driver: ApolloDriver,
      // Setup as per https://the-guild.dev/graphql/mesh/docs/getting-started/customize-mesh-server
      useFactory: async () => {
        const { schema, getEnveloped } = await getBuiltMesh();

        return {
          schema,
          async executor(requestContext) {
            const { schema, execute, contextFactory } = getEnveloped({
              req: requestContext.request.http,
            });

            return execute({
              schema,
              document: requestContext.document,
              contextValue: await contextFactory(),
              variableValues: requestContext.request.variables,
              operationName: requestContext.operationName,
            });
          },
        };
      },
    }),
  ],
})
export class AppModule {}
