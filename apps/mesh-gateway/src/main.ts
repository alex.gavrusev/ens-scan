import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  const port = 4000;
  await app.listen(port);
  Logger.log(`Application is running on: ${await app.getUrl()}`);
}

bootstrap();
